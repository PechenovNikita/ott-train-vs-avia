var React = require('react')
  , ReactDOM = require('react-dom');
var TimerMixin = require('react-timer-mixin');

var Main = require('./cards/main.jsx')
  , Card = require('./cards/card.jsx')
  , Finish = require('./cards/finish.jsx')
  , Email = require('./cards/email.jsx');

var Share = require('./../js/Share');
var Rules = require('./some-blocks/rules.jsx');

var Footer = require('./some-blocks/footer.jsx');

function animate(elem, style, unit, from, to, time, prop) {
  if (!elem) return;
  var start = new Date().getTime(),
    timer = setInterval(function () {
      var step = Math.min(1, (new Date().getTime() - start) / time);
      if (prop) {
        elem[style] = (from + step * (to - from)) + unit;
      } else {
        elem.style[style] = (from + step * (to - from)) + unit;
      }
      if (step == 1) clearInterval(timer);
    }, 25);
  elem.style[style] = from + unit;
}

//animate(document.body, "scrollTop", "", 0, this.refs.cards.offsetTop + 10, 300, true);
function aniScroll(from, to, time) {
  var start = new Date().getTime(),
    timer = setInterval(function () {
      var step = Math.min(1, (new Date().getTime() - start) / time);
      document.body.scrollTop = (from + step * (to - from));
      if (step == 1) clearInterval(timer);
    }, 25);
}

var Data = require('./../js/_data');

var whenShareResult = function () {

};
var global_result = 0;

var default_title = 'Конкурс Самолет vs Поезд с OneTwoTrip!';
var default_text = 'Пройди интересный тест OneTwoTrip и получи бесплатный билет туда и обратно по РФ! Без призов никто не останется.';
var default_site_name = 'Конкурс Самолет vs Поезд с OneTwoTrip';


document.addEventListener('click', function (event) {
  target = event.target;
  while (target.parentNode && target != document.body) {
    if (target.classList.contains('block-btn-social')) {
      event.stopPropagation();
      event.preventDefault();
      var url = 'https://www.onetwotrip.com/promo/railways2016/';
      var type = target.getAttribute('data-social');

      if (target.hasAttribute('data-result') && type !== 'tw') {
        url = url + 'results/' + global_result + "-5.html";
      }

      if (type == 'tw') {
        Share['tw'](url, 'Самолет VS Поезд. Проведем тест драйв и выясним, кто быстрее, больше и мощнее!');
      } else
        Share[type](url);

      if (ga)
        ga('send', 'event', 'rails2016', 'share', type);

      // if (target.hasAttribute('data-result')) {
      //   setTimeout(whenShareResult, 3000);
      // }

      return false;
    } else if (target.hasAttribute('data-link') && target.getAttribute('data-link') == 'rules') {
      document.querySelector('.block-rules').classList.add('open');
      return false;
    }
    target = target.parentNode;
  }
});

var RZHDC = React.createClass({
  mixins                : [TimerMixin],
  getInitialState       : function () {
    this.result = 0;
    return {
      share        : false,
      question     : -1,
      mob_testAcc  : 0,
      mob_startAcc : 0
    };
  },
  whenShareResult       : function () {
    var st = this.state;
    st.share = true;
    this.setState(st);
  },
  componentDidMount     : function () {
    //document.addEventListener('touchstart', function (event) {
    //  console.log(event.target);
    //}, true);
    var self = this;

    document.addEventListener('mousemove', function (event) {
      var w = (window.innerWidth >= 820) ? 400 : 200;
      self.parallax((event.clientX - (window.innerWidth - w) / 2) / w);
    });

    //document.addEventListener('touchmove', function (event) {
    //  var w = (window.innerWidth >= 820) ? 400 : 200;
    //  self.parallax((event.touches[0].clientX - (window.innerWidth - w) / 2) / w);
    //});

    window.addEventListener("deviceorientation", this.mob__getOrientFromAcc, true);

    this.refs.main.whenStart(this.start);
    whenShareResult = this.whenShareResult;

    // @TODO  Remove
    // this.refs.main.start();
    // this.next();
    // this.next();
    // this.next();
    // this.next();
    // this.next();
    // this.whenShareResult();
    //
    // window.test = this.refs.finish;
  },
  mob__getOrient        : function () {
    if (window.orientation) {
      var current = this.mob_orient;
      switch (window.orientation) {
        case -90:
        case 90:
          this.mob_orient = 'landscape';
          break;
        default:
          this.mob_orient = 'portrait';
          break;
      }
      if (current != this.mob_orient)
        this.mob_startAcc = false;
    }
  },
  mob__getOrientFromAcc : function (event) {
    this.mob__getOrient();

    var st = this.state
      , deg = (this.mob_orient === 'landscape') ? event.beta : event.gamma;
    deg = Math.round(deg * 10) / 10;

    if (!this.mob_startAcc) {
      this.mob_startAcc = deg;
      st.mob_startAcc = this.mob_startAcc;
    }

    this.mob_testAcc = Math.round((15 + (this.mob_startAcc - deg)) / 30 * 10) / 10;
    this.mob_testAcc = ( this.mob_testAcc < 0 ) ? 0 : this.mob_testAcc;
    this.mob_testAcc = ( this.mob_testAcc > 1 ) ? 1 : this.mob_testAcc;

    st.mob_testAcc = this.mob_testAcc;

    this.setState(st);
    this.parallax(this.mob_testAcc);

  },
  parallax              : function (pr) {
    if (this.state.question === -1)
      this.refs.main.setParallaxProgress(pr);
    else {
      //this.refs["question_" + this.state.question].setParallaxProgress(pr);
    }
  },
  start                 : function () {
    this.next();
    aniScroll(document.body.scrollTop, this.refs.cards.offsetTop + 10, 200);
  },
  whenAnswer            : function (result) {
    if (result) {
      this.result++;
      global_result++;
    }
    this.refs.footer.enableNext(this.state.question >= (Data.questions.length - 1));
  },
  next                  : function (event) {
    if (event && event.nativeEvent) {
      event.nativeEvent.preventDefault();
      event.nativeEvent.stopPropagation();
    }
    if (this.state.question > -1 && this.state.question < Data.questions.length)
      this.refs['question_' + this.state.question].hide();

    var st = this.state;
    st.question++;
    this.setState(st);

    if (this.state.question > -1 && this.state.question < Data.questions.length)
      this.refs['question_' + this.state.question].show();

    this.refs.footer.next();
    aniScroll(document.body.scrollTop, this.refs.cards.offsetTop + 10, 200);
  },
  render                : function () {

    var marginLeft = ((this.state.question + 1) * -100) + "%";
    var questions = [];

    for (var index = 0; index < Data.questions.length; index++) {
      questions.push((
        <li key={index} className="block-cards__list__item">
          <Card ref={"question_"+index} data={Data.questions[index]} index={index}
                onAnswer={this.whenAnswer}
          />
        </li>
      ))
    }

    var finishCard = '';

    if (!this.state.share && this.state.question >= Data.questions.length)
      finishCard = <Finish ref="finish" right_answer={this.result} count={Data.questions.length}/>;
    else if (this.state.share)
      finishCard = <Email />;

    return (
      <div ref="cards" className="block-cards">
        <ul className="block-cards__list" style={{marginLeft:marginLeft}}>
          <li className="block-cards__list__item">
            <Main ref="main"/>
          </li>
          {questions}
          <li className="block-cards__list__item">
            {finishCard}
          </li>
        </ul>
        <div style={{display:"inline-block"}}></div>
        <Footer ref="footer" count={Data.questions.length} onNextClick={this.next}/>
      </div>
    );
  }
});

ReactDOM.render(
  <RZHDC/>,
  document.getElementById('block_cards')
);

ReactDOM.render(
  <Rules/>,
  document.getElementById('block_modal')
);
