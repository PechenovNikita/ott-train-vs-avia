var React = require('react')
  , ReactDOM = require('react-dom');
 

var Share = require('./../js/Share');
var Rules = require('./some-blocks/rules.jsx');
 

document.addEventListener('click', function (event) {
  target = event.target;
  while (target.parentNode && target != document.body) {
    if (target.classList.contains('block-btn-social')) {
      event.stopPropagation();
      event.preventDefault();
      var url = 'https://www.onetwotrip.com/promo/railways2016/';
      var type = target.getAttribute('data-social');

      if (type == 'tw') {
        Share['tw'](url, 'Самолет VS Поезд. Проведем тест драйв и выясним, кто быстрее, больше и мощнее!');
      } else
        Share[type](url);

      return false;
    } else if (target.hasAttribute('data-link') && target.getAttribute('data-link') == 'rules') {
      document.querySelector('.block-rules').classList.add('open');
      return false;
    }
    target = target.parentNode;
  }
});



ReactDOM.render(
  <Rules/>,
  document.getElementById('block_modal')
);
