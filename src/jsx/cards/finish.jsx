var React = require('react');

function getNumberImage(num) {
  switch (num) {
    case 1:
      return (<img src="./img/finish/num-1.png"/>);
    case 2:
      return (<img src="./img/finish/num-2.png"/>);
    case 3:
      return (<img src="./img/finish/num-3.png"/>);
    case 4:
      return (<img src="./img/finish/num-4.png"/>);
    case 5:
      return (<img src="./img/finish/num-5.png"/>);
    case 0:
    default:
      return (<img src="./img/finish/num-0.png"/>);
  }
}

var FinishCard = React.createClass({
  getInitialState : function () {
    return {
      right : (this.props.right_answer) ? this.props.right_answer : 0
    };
  },
  addRight        : function () {
    this.setState({
      right : (this.state.right + 1)
    });
  },
  render          : function () {

    var topText = (this.state.right <= 3)
      ? 'Не расстраивайся, зато теперь ты знаешь ответы на самые коварные вопросы о поездах и самолетах!'
      : 'Отлично! Ты наверное пилот или машинист. В любом случае, ты просто профи во всём, что касается поездов и самолётов.';

    // var socialText = 'Расскажи об этом своим друзьям и выиграй 2 билета на поезд или промо код на бронирование отелей на 1500 рублей!';
    var socialText = 'Расскажи об этом своим друзьям!';

    return (
      <div className="block-card">
        <div className="block-card-finish">
          <div className="block-card__content block-card-finish__content">
            <div className="block-card-finish__result">
              <div className="block-card-finish__result__head">
                <img src="./img/finish/logo.png" alt="" className="block-card-finish__result__head__logo"/>

                <div className="block-card-finish__result__head__icons">
                  <div className="block-card-finish__result__head__icons__icon">
                    <img src="./img/icon--air.png" alt=""
                         className="block-card-finish__result__head__icons__icon__img"/>
                  </div>
                  <div className="block-card-finish__result__head__icons__delimiter">
                    vs
                  </div>
                  <div className="block-card-finish__result__head__icons__icon">
                    <img src="./img/icon--train.png" alt=""
                         className="block-card-finish__result__head__icons__icon__img"/>
                  </div>
                </div>
              </div>
              <div className="block-card-finish__result__content">
                <div className="block-finish-result">
                  <div className="block-finish-result__right">
                    {getNumberImage(this.state.right)}
                  </div>
                  <div className="block-finish-result__delimiter">/</div>
                  <div className="block-finish-result__all">
                    {getNumberImage(this.props.count)}
                  </div>
                </div>
                <div className="block-card-finish__result__content__title">Первым делом самолеты!</div>
                <div className="block-card-finish__result__content__text">{topText}</div>
              </div>
            </div>
            <div className="block-card-finish__share">
              <span className="block-card-finish__share__text">{socialText}</span>
              <span className="block-card-finish__share__social">
                <a href="#" data-social="fb" data-result="true"
                   className="block-card-finish__share__social__icon block-btn-social block-btn-social_fb">
                  <span className="block-btn-social__icon"></span>
                </a>
                <a href="#" data-social="vk" data-result="true"
                   className="block-card-finish__share__social__icon block-btn-social block-btn-social_vk">
                  <span className="block-btn-social__icon"></span>
                </a>
                <a href="#" data-social="tw" data-result="true"
                   className="block-card-finish__share__social__icon block-btn-social block-btn-social_tw">
                  <span className="block-btn-social__icon"></span>
                </a>
              </span>
            </div>
          </div>
        </div>
      </div>
    );
  }
});

module.exports = FinishCard;