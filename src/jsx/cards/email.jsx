var path = '/promo/railways2016/';
var currentDomain = 'http://www.onetwotrip.com';

var config = {
  confirm     : 'https://www.onetwotrip.com/_api/visitormanager/registerPromo/',
  valideEmail : 'https://www.onetwotrip.com/_api/emailvalidator/validate/',
  url         : currentDomain + path,
  redirectURL : currentDomain + path + 'confirm.html',
  source      : 'TA_RZHDVSAVIA'
};

var Final = (function () {
  'use strict';

  return React.createClass({
    getInitialState     : function () {
      return {
        done    : false,
        error   : false,
        disable : false,
        form    : true
      };
    },
    componentDidMount   : function () {

    },
    form                : function () {
      var st = this.state;
      st.form = true;
      this.setState(st);
    },
    email               : function (event) {
      this.disable();
      event.stopPropagation();
      event.preventDefault();
      var email = this.refs.email_input.value;

      if (this.validateEmail(email)) {
        this.validateEmailByAjax(email);
      } else
        this.formError();

    },
    validateEmail       : function (email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    },
    validateEmailByAjax : function (email) {
      jQuery.ajax({
        url      : config.valideEmail,
        data     : {
          email : email
        },
        dataType : "jsonp"
      }).done(this.__emailCheckAjax);

      if (ga)
        ga('send', 'event', 'rails2016', 'email_sent', email);
    },
    __emailCheckAjax    : function (response) {
      if (response && response.isValid) {
        jQuery.ajax({
            url      : config.confirm,
            data     : {
              source      : config.source,
              email       : this.refs.email_input.value,
              redirectURL : config.redirectURL
            },
            dataType : "jsonp"
          })
          .done(this.done);
      } else
        this.formError();
    },
    formError           : function () {
      this.undisable();
      var st = this.state;
      st.error = true;
      this.setState(st);
    },
    formClear           : function () {
      var st = this.state;
      st.error = false;
      this.setState(st);
    },
    done                : function (response) {
      // console.log(response);
      if (typeof response == 'string')
        response = JSON.parse(response);
      if (!response || response.result != 'OK') {
        this.formError();
      } else {
        window.location = config.redirectURL;
      }
    },
    disable             : function () {
      var st = this.state;
      st.disable = true;
      this.setState(st);
    },
    undisable           : function () {
      var st = this.state;
      st.disable = false;
      this.setState(st);
    },
    render              : function () {
      return (
        <div className="block-email">
          <div className="block-email__title">
            <span className="block-email__title__inner">Спасибо за участие!</span>
          </div>
          <form onSubmit={this.email}
                onInput={this.formClear}
                ref="email_form" method="post"
                className={"block-email__form"+(this.state.error?' error':"")}>
            <input type="hidden" name="source" value="promo_euro2016"/>
            <input type="hidden" name="redirectURL" value="https://www.onetwotrip.com/promo/newyear2016/confirm.html"/>
            <div className="block-email__form__desc">
              <span className="block-email__form__desc__inner">Оставьте свой электронный адрес и узнайте о результатах соревнования</span>
            </div>
            <div className="block-email__form__control">
              <input ref="email_input"
                     type="text" name="email"
                     placeholder="Введите электронный адрес"
                     className="block-email__form__control__input"/>
            </div>
            <div className="block-email__form__submit">
              <button className="block-email__form__submit__btn"
                      disabled={this.state.disable ? 'disabled' : ''}>
                <span>Отправить</span>
              </button>
            </div>
          </form>
        </div>
      );
    }
  });
}());

module.exports = Final;
