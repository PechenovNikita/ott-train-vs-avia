var React = require('react');

var SelectButtons = require('./select-btn.jsx');

var QuestionCard = React.createClass({
  selectTrain : function () {
    this.props.onSelectTrain();
  },
  selectAir   : function () {
    this.props.onSelectAir();
  },
  render      : function () {
    return (
      <div className="block-card-question">
        <div className="block-card__content block-card-question__content">
          <div className="block-card-question__back">
            <img src={this.props.back} alt="" className="block-card-question__back__img block-card-back-img"/>
          </div>
          <div className="block-card-question__number">
            <img src={this.props.number} alt="" className="block-card-question__number__img"/>
          </div>
          <div className="block-card-question__task">
            <div className="block-card-question__task__title">
              {this.props.text}
            </div>
            <div className="block-card-question__task__select">
              Выберите:
            </div>
            <div className="block-card-question__task__btns">
              <SelectButtons onSelectAir={this.selectAir} onSelectTrain={this.selectTrain}/>
            </div>
          </div>
        </div>
      </div>
    );
  }
});

module.exports = QuestionCard;