var React = require('react');

var SelectButtons = React.createClass({
  render          : function () {
    return (
      <div className="block-card-select">
        <button className="block-card-select__btn block-card-select--air" onClick={this.props.onSelectAir}>
          <span className="block-card-select__btn__icon">
            <img src="./img/icon--air.png" alt="" className="block-card-select__btn__icon__img"/>
          </span>
          <span className="block-card-select__btn__text">Самолет</span>
        </button>
        <button className="block-card-select__btn block-card-select--train" onClick={this.props.onSelectTrain}>
          <span className="block-card-select__btn__icon">
            <img src="./img/icon--train.png" alt="" className="block-card-select__btn__icon__img"/>
          </span>
          <span className="block-card-select__btn__text">Поезд</span>
        </button>
      </div>
    );
  }
});

module.exports = SelectButtons;