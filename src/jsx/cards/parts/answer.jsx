var React = require('react');

var AnswerCard = React.createClass({
  getInitialState : function () {
    return {
      success : true
    };
  },
  setResult       : function (result) {
    this.setState({
      success : result
    });
  },
  click           : function () {
    this.props.onClick();
  },
  render          : function () {
    return (
      <div className="block-card-answer" onClick={this.click}>
        <div className="block-card-answer__back">
          <img src={this.props.back} alt="" className="block-card-answer__back__img block-card-back-img"/>
        </div>
        <div className="block-card-answer__icons">
          <div className="block-card-answer__icons__rights">
            {(()=> {
              if (this.props.showAir) {
                return (
                  <div className="block-card-answer__icons__rights__icon">
                    <img src="./img/icon--air.png" alt=""
                         className="block-card-answer__icons__rights__icon__img"/>
                  </div>
                );
              }
            })()}
            {(()=> {
              if (this.props.showTrain) {
                return (
                  <div className="block-card-answer__icons__rights__icon">
                    <img src="./img/icon--train.png" alt=""
                         className="block-card-answer__icons__rights__icon__img"/>
                  </div>
                );
              }
            })()}
          </div>
          {(()=> {
            if (this.state.success) {
              return (
                <div className="block-card-answer__icons__result">
                  <img src="./img/questions/q_answer--success.png" alt=""
                       className="block-card-answer__icons__result__img"/>
                </div>
              );
            } else {
              return (
                <div className="block-card-answer__icons__result">
                  <img src="./img/questions/q_answer--fail.png" alt=""
                       className="block-card-answer__icons__result__img"/>
                </div>
              );
            }
          })()}
        </div>
        <div className="block-card-answer__desc">
          <div className="block-card-answer__desc__text">{this.props.text}</div>
        </div>
      </div>
    );
  }
});

module.exports = AnswerCard;