var React = require('react');
var TimerMixin = require('react-timer-mixin');

var Question = require('./parts/question.jsx')
  , Answer = require('./parts/answer.jsx');

var Card = React.createClass({
  mixins              : [TimerMixin],
  getInitialState     : function () {
    return {
      animate   : true,
      parallax  : 0,
      answer    : false,
      success   : true,
      hideLeft  : false,
      hideRight : true,
      display   : false
    };
  },
  show                : function () {
    var st = this.state;
    st.display = true;
    st.hideRight = false;
    this.setState(st);
  },
  hide                : function () {
    var st = this.state;
    st.hideRight = false;
    st.hideLeft = true;
    this.setState(st);
    this.setTimeout(
      () => {
        var s = this.state;
        s.display = false;
        this.setState(s);
      },
      400
    );
  },
  componentDidMount   : function () {
    //this.selectAir();
  },
  selectTrain         : function () {
    var st = this.state;
    st.answer = true;
    this.refs.answer.setResult(this.props.data.train);
    this.setState(st);

    if (this.props.onAnswer)
      this.props.onAnswer(this.props.data.train);
  },
  selectAir           : function () {
    var st = this.state;
    st.answer = true;
    this.refs.answer.setResult(this.props.data.air);
    this.setState(st);

    if (this.props.onAnswer)
      this.props.onAnswer(this.props.data.air);
  },
  setParallaxProgress : function (pr) {
    var st = this.state;
    st.parallax = Math.round(pr * 100) / 100;
    this.setState(st);
  },
  getParallaxProgress : function () {
    return this.state.parallax;
  },
  answerClick         : function () {
    if (this.state.answer)
      this.props.onAnswerClick();
  },
  render              : function () {
    var classes = "block-card block-card-question-" + (this.props.index + 1)
      + ((this.state.animate) ? " animate" : "")
      + ((this.state.answer) ? " show-answer" : "")
      + ((this.state.hideLeft) ? " hide-left" : "")
      + ((this.state.hideRight) ? " hide-right" : "");

    if (!this.state.hide && !this.state.hideRight && this.state.parallax != 0) {
      var deg = (this.state.parallax * 2 - 1) * .5;
      var transform = 'rotateY(' + deg + "deg) translate(" + -1 * deg * 10 + "px, 0)";

      var styles = {
        '-webkit-transform' : transform,
        '-moz-transform'    : transform,
        '-ms-transform'     : transform,
        '-o-transform'      : transform,
        'transform'         : transform
      };

    } else
      styles = {};

    styles.display = (this.state.display ? 'block' : 'none');

    return (
      <div className={classes} style={styles}>
        <Question back={this.props.data.back} number={this.props.data.number}
                  text={this.props.data.question}
                  onSelectTrain={this.selectTrain} onSelectAir={this.selectAir}/>
        <Answer ref="answer"
                onClick={this.answerClick}
                back={this.props.data.back} text={this.props.data.answer}
                showTrain={this.props.data.train} showAir={this.props.data.air}/>
      </div>
    );
  }
});

module.exports = Card;