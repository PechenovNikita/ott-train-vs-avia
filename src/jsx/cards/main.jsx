var React = require('react');
var TimerMixin = require('react-timer-mixin');

var pfx = ["webkit", "moz", "MS", "o", ""];
function PrefixedEvent(element, type, callback) {
  for (var p = 0; p < pfx.length; p++) {
    if (!pfx[p]) type = type.toLowerCase();
    element.addEventListener(pfx[p] + type, callback, false);
  }
}

var MainCard = React.createClass({
  mixins              : [TimerMixin],
  getInitialState     : function () {

    return {
      title        : 'Ответь на 5 вопросов и прими участие в розыгрыше 2х билетов на поезд.',
      parallax     : 0.5,
      playParallax : false,
      hide         : false,
      animate      : false,
      display      : false
    };
  },
  componentDidMount   : function () {
    var self = this;
    // PrefixedEvent(this.refs.main, 'AnimationEnd', function (event) {
    //   self.stopAnimation();
    // });
    var st = this.state;
    st.display = true;
    this.setState(st);
  },
  stopAnimation       : function () {
    var st = this.state;
    st.playParallax = true;
    this.setState(st);
  },
  setParallaxProgress : function (pr) {
    var st = this.state;
    st.parallax = Math.round(pr * 100) / 100;
    this.setState(st);
  },
  getParallaxProgress : function () {
    return this.state.parallax;
  },
  whenStart           : function (callback) {
    this.onStart = callback;
  },
  start               : function (event) {
    if (event) {
      event.stopPropagation();
      event.preventDefault();
    }
    this.hideLeft();
    if (this.onStart) {
      this.onStart();
    }
  },
  hideLeft            : function () {
    var st = this.state;
    st.animate = true;
    st.hide = true;
    this.setState(st);
    this.setTimeout(
      () => {
        var s = this.state;
        s.display = false;
        this.setState(s);
      },
      400
    );
  },
  render              : function () {

    if (!this.state.hide && this.state.playParallax) {
      var deg = this.state.parallax * 2 - 1;
      var transform = 'rotateY(' + deg + "deg) translate(" + -1 * deg * 10 + "px, 0)";

      var styles = {
        'WebkitTransform' : transform,
        'MozTransform'    : transform,
        'MsTransform'     : transform,
        'OTransform'      : transform,
        'transform'       : transform
      };

    } else
      styles = {};

    styles.display = (this.state.display ? 'block' : 'none');

    if (!this.state.hide && this.state.playParallax) {
      var trPr = Math.min(Math.max(0, this.state.parallax), 1)
        , airPr = Math.min(Math.max(0, 1 - this.state.parallax), 1);
      var trZ = ((trPr > airPr) ? 1 : 0)
        , airZ = ((trPr > airPr) ? 0 : 1);
      var train_style = {
        opacity : trPr,
        zIndex  : trZ
      }
        , air_style = {
        opacity : airPr,
        zIndex  : airZ
      };
    } else {
      train_style = {};
      air_style = {};
    }

    var cl = "block-card" +
      (this.state.hide ? " hide-left" : "") +
      (this.state.animate ? " animate" : "");

    return (
      <div className={cl} style={styles}>

        <div className="block-card__overflow">
          <div className="block-card__overflow__title">
            <span className="block-card__overflow__title__inner">Конкурс завершен 14 августа 2016 года</span>
          </div>
          <div className="block-card__overflow__text">
            <span className="block-card__overflow__text__inner">Если вам интересно посмотреть как это было, вы можете пройти конкурс еще раз или перейти к поиску путешествий.</span>
          </div>

          <div className="block-card__overflow__btns">
          
            <button onClick={this.start} onTouchStart={this.start} className="block-card-main__footer__btn__start">
              <span>ПРОЙТИ ТЕСТ</span>
            </button>

            <a href="https://onetwotrip.com" className="block-card__overflow__btns__btn block-card-main__footer__btn__start">
              <span>ПЕРЕЙТИ НА САЙТ</span>
            </a>
          </div>
        </div>

        <div ref="main" className="block-card-main">


          <div className="block-card__content block-card-main__content">
            <div className="block-card-main__logo">
              <img src="./img/main/logo.png" alt="vs" className="block-card-main__logo__img"/>
            </div>
            <div className="block-card-main__title">
              <h2 className="block-card-main__title__heading">{this.state.title}</h2>
            </div>
            <div className="block-card-main__or">
              <div className="block-main-or--is-left block-main-or block-main-air" style={air_style}>
                <div className="block-main-or__name">
                  <span onClick={this.start} onTouchStart={this.start}>самолет</span>
                </div>
                <div className="block-main-or__picture">
                  <img src="./img/main/img--air.png" alt="" className="block-main-or__picture__img"/>
                </div>
              </div>
              <div className="block-main-or--is-right block-main-or  block-main-train" style={train_style}>
                <div className="block-main-or__picture">
                  <img src="./img/main/img--train.png" alt="" className="block-main-or__picture__img"/>
                </div>
                <div className="block-main-or__name">
                  <span onClick={this.start} onTouchStart={this.start}>поезд</span>
                </div>
              </div>
            </div>
          </div>
          <div className="block-card__footer block-card-main__footer">
            <div className="block-card-main__footer__btn">

            </div>
          </div>
        </div>
      </div>
    );
  }
});

module.exports = MainCard;
