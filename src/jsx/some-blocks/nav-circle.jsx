var React = require('react');

var NavCircle = React.createClass({
  getInitialState : function () {
    return {
      current : !!this.props.current,
      done    : !!this.props.done
    };
  },
  current         : function () {
    var st = this.state;
    st.current = true;
    this.setState(st);
  },
  done            : function () {
    var st = this.state;
    st.current = false;
    st.done = true;
    this.setState(st);

  },
  render          : function () {
    var cl = "block-nav-circle"
    + ((this.state.current) ? " block-nav-circle--is-current" : "")
    + ((this.state.done) ? " block-nav-circle--is-done" : "");

    return (
      <div className={cl}></div>
    )
  }
});

module.exports = NavCircle;