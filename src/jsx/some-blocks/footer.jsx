var React = require('react');

var NavCircle = require('./nav-circle.jsx');

var Footer = React.createClass({

  getInitialState : function () {
    this.current = -1;
    return {
      end          : false,
      disable_next : true,
      showNav      : false
    };
  },

  next       : function () {
    if (this.current >= 0 && this.current < this.props.count)
      this.refs['circle_' + this.current].done();
    this.current++;
    if (this.current >= 0 && this.current < this.props.count) {
      this.showNav();
      this.refs['circle_' + this.current].current();
    } else
      this.hideNav();

  },
  showNav    : function () {
    this.setState({
      end          : this.state.end,
      disable_next : true,
      showNav      : true
    });
  },
  hideNav    : function () {
    this.setState({
      end          : this.state.end,
      disable_next : true,
      showNav      : false
    });
  },
  enableNext : function (end) {
    this.setState({
      end          : ((typeof end !== 'undefined') ? end : this.state.end),
      disable_next : false,
      showNav      : true
    });
  },
  clickNext  : function (event) {
    if (!this.state.disable_next) {
      event.stopPropagation();
      event.preventDefault();
      if (this.props.onNextClick)
        this.props.onNextClick();
    }
  },
  render     : function () {
    var circles = [];

    for (var i = 0; i < this.props.count; i++) {
      circles.push((
        <li key={i} className="block-cards__footer__nav__items__item">
          <NavCircle ref={"circle_"+i} current={i === this.current} done={i < this.current}/>
        </li>
      ));
    }

    var classForFooter = 'block-cards__footer'
      + (this.state.showNav ? ' show-nav' : '')
      + (!this.state.disable_next ? ' show-next' : '');

    var btn_text = (this.state.end)?'Результат':'Следующий';

    return (

      <div className={classForFooter}>
        <div className="block-cards__footer__next">
          <button className="block-cards__footer__next__btn"
                  disabled={this.state.disable_next}
                  onTouchStart={this.clickNext}
                  onClick={this.clickNext}>
            <span>{btn_text}</span>
          </button>
        </div>
        <div className="block-cards__footer__nav">
          <ul className="block-cards__footer__nav__items">
            {circles}
          </ul>
        </div>
        <div className="block-cards__footer__links">
          <a href="#" data-link="rules" className="block-cards__footer__links__link">Правила участия</a>
        </div>
      </div>
    );
  }
});

module.exports = Footer;
