var gulp = require('gulp')
  , jade = require('gulp-jade')
  , less = require('gulp-less')
  , livereload = require('gulp-livereload')
  , react = require('gulp-react')
  , webpack = require("gulp-webpack");

gulp.task('webpack', function () {

  gulp.src('./src/js/jsx/app.jsx')
    .pipe(webpack(require('./webpack.config.js')))
    .pipe(gulp.dest('./dist/js'))
    .pipe(livereload());

});

gulp.task('jade', function () {
  gulp.src('./src/jade/index.jade')
    .pipe(jade({
      pretty : true,
      locals : {
        config : {
          url : 'http://www.onetwotrip.com/promo/railways2016/'
        }
      }
    }))
    .pipe(gulp.dest('./dist'))
    .pipe(livereload());
  gulp.src('./src/jade/results/*.jade')
    .pipe(jade({
      pretty : true,
      locals : {
        config : {
          url : 'http://www.onetwotrip.com/promo/railways2016/'
        }
      }
    }))
    .pipe(gulp.dest('./dist/results'))
    .pipe(livereload());
});

gulp.task('js', function () {
  gulp.src('./src/js/**/*.js')
    .pipe(gulp.dest('./dist/js'))
    .pipe(livereload());
});

gulp.task('less', function () {
  gulp.src('./src/less/index.less')
    .pipe(less())
    .pipe(gulp.dest('./dist/css'))
    .pipe(livereload());
});

gulp.task('default', function () {
  livereload.listen();
  gulp.start(['less', 'js', 'webpack', 'jade']);

  gulp.watch(['./src/jade/**/*.jade'], ['jade']);
  //gulp.watch(['./src/js/**/*.js'], ['js']);
  gulp.watch(['./src/jsx/**/*.jsx', './src/jsx/**/*.js'], ['webpack']);
  gulp.watch(['./src/less/**/*.less'], ['less']);
});
