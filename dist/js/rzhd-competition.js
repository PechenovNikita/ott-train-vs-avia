/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx React.DOM */var React = __webpack_require__(1)
	  , ReactDOM = __webpack_require__(2);
	var TimerMixin = __webpack_require__(5);
	
	var Main = __webpack_require__(6)
	  , Card = __webpack_require__(7)
	  , Finish = __webpack_require__(11)
	  , Email = __webpack_require__(12);
	
	var Share = __webpack_require__(3);
	var Rules = __webpack_require__(4);
	
	var Footer = __webpack_require__(13);
	
	function animate(elem, style, unit, from, to, time, prop) {
	  if (!elem) return;
	  var start = new Date().getTime(),
	    timer = setInterval(function () {
	      var step = Math.min(1, (new Date().getTime() - start) / time);
	      if (prop) {
	        elem[style] = (from + step * (to - from)) + unit;
	      } else {
	        elem.style[style] = (from + step * (to - from)) + unit;
	      }
	      if (step == 1) clearInterval(timer);
	    }, 25);
	  elem.style[style] = from + unit;
	}
	
	//animate(document.body, "scrollTop", "", 0, this.refs.cards.offsetTop + 10, 300, true);
	function aniScroll(from, to, time) {
	  var start = new Date().getTime(),
	    timer = setInterval(function () {
	      var step = Math.min(1, (new Date().getTime() - start) / time);
	      document.body.scrollTop = (from + step * (to - from));
	      if (step == 1) clearInterval(timer);
	    }, 25);
	}
	
	var Data = __webpack_require__(15);
	
	var whenShareResult = function () {
	
	};
	var global_result = 0;
	
	var default_title = 'Конкурс Самолет vs Поезд с OneTwoTrip!';
	var default_text = 'Пройди интересный тест OneTwoTrip и получи бесплатный билет туда и обратно по РФ! Без призов никто не останется.';
	var default_site_name = 'Конкурс Самолет vs Поезд с OneTwoTrip';
	
	
	document.addEventListener('click', function (event) {
	  target = event.target;
	  while (target.parentNode && target != document.body) {
	    if (target.classList.contains('block-btn-social')) {
	      event.stopPropagation();
	      event.preventDefault();
	      var url = 'https://www.onetwotrip.com/promo/railways2016/';
	      var type = target.getAttribute('data-social');
	
	      if (target.hasAttribute('data-result') && type !== 'tw') {
	        url = url + 'results/' + global_result + "-5.html";
	      }
	
	      if (type == 'tw') {
	        Share['tw'](url, 'Самолет VS Поезд. Проведем тест драйв и выясним, кто быстрее, больше и мощнее!');
	      } else
	        Share[type](url);
	
	      if (ga)
	        ga('send', 'event', 'rails2016', 'share', type);
	
	      // if (target.hasAttribute('data-result')) {
	      //   setTimeout(whenShareResult, 3000);
	      // }
	
	      return false;
	    } else if (target.hasAttribute('data-link') && target.getAttribute('data-link') == 'rules') {
	      document.querySelector('.block-rules').classList.add('open');
	      return false;
	    }
	    target = target.parentNode;
	  }
	});
	
	var RZHDC = React.createClass({displayName: "RZHDC",
	  mixins                : [TimerMixin],
	  getInitialState       : function () {
	    this.result = 0;
	    return {
	      share        : false,
	      question     : -1,
	      mob_testAcc  : 0,
	      mob_startAcc : 0
	    };
	  },
	  whenShareResult       : function () {
	    var st = this.state;
	    st.share = true;
	    this.setState(st);
	  },
	  componentDidMount     : function () {
	    //document.addEventListener('touchstart', function (event) {
	    //  console.log(event.target);
	    //}, true);
	    var self = this;
	
	    document.addEventListener('mousemove', function (event) {
	      var w = (window.innerWidth >= 820) ? 400 : 200;
	      self.parallax((event.clientX - (window.innerWidth - w) / 2) / w);
	    });
	
	    //document.addEventListener('touchmove', function (event) {
	    //  var w = (window.innerWidth >= 820) ? 400 : 200;
	    //  self.parallax((event.touches[0].clientX - (window.innerWidth - w) / 2) / w);
	    //});
	
	    window.addEventListener("deviceorientation", this.mob__getOrientFromAcc, true);
	
	    this.refs.main.whenStart(this.start);
	    whenShareResult = this.whenShareResult;
	
	    // @TODO  Remove
	    // this.refs.main.start();
	    // this.next();
	    // this.next();
	    // this.next();
	    // this.next();
	    // this.next();
	    // this.whenShareResult();
	    //
	    // window.test = this.refs.finish;
	  },
	  mob__getOrient        : function () {
	    if (window.orientation) {
	      var current = this.mob_orient;
	      switch (window.orientation) {
	        case -90:
	        case 90:
	          this.mob_orient = 'landscape';
	          break;
	        default:
	          this.mob_orient = 'portrait';
	          break;
	      }
	      if (current != this.mob_orient)
	        this.mob_startAcc = false;
	    }
	  },
	  mob__getOrientFromAcc : function (event) {
	    this.mob__getOrient();
	
	    var st = this.state
	      , deg = (this.mob_orient === 'landscape') ? event.beta : event.gamma;
	    deg = Math.round(deg * 10) / 10;
	
	    if (!this.mob_startAcc) {
	      this.mob_startAcc = deg;
	      st.mob_startAcc = this.mob_startAcc;
	    }
	
	    this.mob_testAcc = Math.round((15 + (this.mob_startAcc - deg)) / 30 * 10) / 10;
	    this.mob_testAcc = ( this.mob_testAcc < 0 ) ? 0 : this.mob_testAcc;
	    this.mob_testAcc = ( this.mob_testAcc > 1 ) ? 1 : this.mob_testAcc;
	
	    st.mob_testAcc = this.mob_testAcc;
	
	    this.setState(st);
	    this.parallax(this.mob_testAcc);
	
	  },
	  parallax              : function (pr) {
	    if (this.state.question === -1)
	      this.refs.main.setParallaxProgress(pr);
	    else {
	      //this.refs["question_" + this.state.question].setParallaxProgress(pr);
	    }
	  },
	  start                 : function () {
	    this.next();
	    aniScroll(document.body.scrollTop, this.refs.cards.offsetTop + 10, 200);
	  },
	  whenAnswer            : function (result) {
	    if (result) {
	      this.result++;
	      global_result++;
	    }
	    this.refs.footer.enableNext(this.state.question >= (Data.questions.length - 1));
	  },
	  next                  : function (event) {
	    if (event && event.nativeEvent) {
	      event.nativeEvent.preventDefault();
	      event.nativeEvent.stopPropagation();
	    }
	    if (this.state.question > -1 && this.state.question < Data.questions.length)
	      this.refs['question_' + this.state.question].hide();
	
	    var st = this.state;
	    st.question++;
	    this.setState(st);
	
	    if (this.state.question > -1 && this.state.question < Data.questions.length)
	      this.refs['question_' + this.state.question].show();
	
	    this.refs.footer.next();
	    aniScroll(document.body.scrollTop, this.refs.cards.offsetTop + 10, 200);
	  },
	  render                : function () {
	
	    var marginLeft = ((this.state.question + 1) * -100) + "%";
	    var questions = [];
	
	    for (var index = 0; index < Data.questions.length; index++) {
	      questions.push((
	        React.createElement("li", {key: index, className: "block-cards__list__item"}, 
	          React.createElement(Card, {ref: "question_"+index, data: Data.questions[index], index: index, 
	                onAnswer: this.whenAnswer}
	          )
	        )
	      ))
	    }
	
	    var finishCard = '';
	
	    if (!this.state.share && this.state.question >= Data.questions.length)
	      finishCard = React.createElement(Finish, {ref: "finish", right_answer: this.result, count: Data.questions.length});
	    else if (this.state.share)
	      finishCard = React.createElement(Email, null);
	
	    return (
	      React.createElement("div", {ref: "cards", className: "block-cards"}, 
	        React.createElement("ul", {className: "block-cards__list", style: {marginLeft:marginLeft}}, 
	          React.createElement("li", {className: "block-cards__list__item"}, 
	            React.createElement(Main, {ref: "main"})
	          ), 
	          questions, 
	          React.createElement("li", {className: "block-cards__list__item"}, 
	            finishCard
	          )
	        ), 
	        React.createElement("div", {style: {display:"inline-block"}}), 
	        React.createElement(Footer, {ref: "footer", count: Data.questions.length, onNextClick: this.next})
	      )
	    );
	  }
	});
	
	ReactDOM.render(
	  React.createElement(RZHDC, null),
	  document.getElementById('block_cards')
	);
	
	ReactDOM.render(
	  React.createElement(Rules, null),
	  document.getElementById('block_modal')
	);


/***/ },
/* 1 */
/***/ function(module, exports) {

	module.exports = React;

/***/ },
/* 2 */
/***/ function(module, exports) {

	module.exports = ReactDOM;

/***/ },
/* 3 */
/***/ function(module, exports) {

	module.exports = (function () {
	  "use strict";
	
	  var share = {};
	
	  share.fb = function (url) {
	    popup('http://www.facebook.com/sharer.php?u=' + url);
	  };
	
	  share.tw = function (url, text) {
	    popup('http://twitter.com/share?url=' + url +
	      '&text=' + text +
	      '&hashtags=onetwotrip_poezda');
	  };
	
	  share.vk = function (url, text, title, image) {
	    if (image)
	      popup('http://vk.com/share.php?url=' + url +
	        '&description=' + text +
	        '&title=' + title +
	        '&image=' + image);
	    else if (title)
	      popup('http://vk.com/share.php?url=' + url +
	        '&description=' + text +
	        '&title=' + title);
	    else if (text)
	      popup('http://vk.com/share.php?url=' + url +
	        '&description=' + text);
	    else
	      popup('http://vk.com/share.php?url=' + url);
	
	  };
	
	  share.ok = function (url, text) {
	
	    popup('http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1' +
	      '&st._surl=' + url +
	      '&st.comments=' + text);
	
	  };
	
	  share.okUrl = function (url) {
	    popup('https://connect.ok.ru/offer?url=' + url);
	  };
	
	  var popup = function (url) {
	
	    var left = (window.innerWidth - 626) / 2
	      , top = (window.innerHeight - 436) / 2;
	
	    left = (left < 0) ? 0 : left;
	    top = (top < 0) ? 0 : top;
	
	    window.open(url, '', 'toolbar=0,status=0,width=626,height=436,left=' + left + ',top=' + top);
	
	  };
	
	  return share;
	}());
	


/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx React.DOM */'use strict';
	
	var React = __webpack_require__(1);
	var ReactDOM = __webpack_require__(2);
	
	var Rules = React.createClass({displayName: "Rules",
	  componentDidMount : function () {
	    var self = this;
	    window.addEventListener('keydown', function (event) {
	      if (event.keyCode == 27)
	        self.hide();
	    });
	  },
	  hide              : function () {
	    document.querySelector('.block-rules').classList.remove('open');
	  },
	  click             : function (event) {
	    var target = event.target;
	    while (!$(target).hasClass('block-rules')) {
	      if ($(target).hasClass('block-rules__content__window'))
	        return true;
	      target = target.parentNode;
	    }
	    this.hide();
	  },
	  render            : function () {
	    return (
	      React.createElement("div", {className: "block-rules", onClick: this.click}, 
	        React.createElement("div", {className: "block-rules__back"}), 
	        React.createElement("div", {className: "block-rules__content"}, 
	          React.createElement("div", {className: "block-rules__content__window"}, 
	            React.createElement("div", {className: "block-rules__content__window__body"}, 
	              React.createElement("div", {className: "block-rules-close", onClick: this.hide}, "×"), 
	              React.createElement("h2", null, "Условия участия в Акции"), 
	              React.createElement("ul", null, 
	                React.createElement("li", null, 
	                  React.createElement("p", null, "Настоящие правила (далее – «Правила») регламентируют порядок участия в акции «Самолет или поезд»" + " " +
	                    "(далее – «Акция»).")
	                ), 
	                React.createElement("li", null, 
	                  React.createElement("p", null, "Организатором Акции является компания OneTwoTriр, далее именуемая Организатор.")
	                ), 
	                React.createElement("li", null, 
	                  React.createElement("p", null, "Акция не является лотерей или иным мероприятием, основанным на риске, и носит исключительно" + " " +
	                    "информационный характер о сайте ", React.createElement("a", {href: "https://www.onetwotrip.com", class: "blue", target: "_blank"}, "www.onetwotrip.com"), " " + " " +
	                    "и" + " " +
	                    "мобильном приложении OneTwoTrip (далее – «Сайт»). ")
	                ), 
	                React.createElement("li", null, 
	                  React.createElement("p", null, "Условия участия в Акции:"), 
	                  React.createElement("ul", null, 
	                    React.createElement("li", null, 
	                      React.createElement("p", null, "Участниками Акции могут являться только дееспособные физические совершеннолетние лица," + " " +
	                        "выполнившие условия ее проведения;")
	                    ), 
	                    React.createElement("li", null, 
	                      React.createElement("p", null, "Необходимо пройти тест на Сайте, оставить действующий адрес электронной почты и поделиться" + " " +
	                        "результатом теста в своем аккаунте в одной из социальных сетей (", 
	                        React.createElement("a", {href: "vk.com", class: "blue", target: "_blank"}, "vk.com"), ", ", 
	                        React.createElement("a", {href: "facebook.com", class: "blue", target: "_blank"}, "facebook.com"), ", ", 
	                        React.createElement("a", {href: "twitter.com", class: "blue", target: "_blank"}, "twitter.com"), ", ", 
	                        React.createElement("a", {href: "odnoklassniki.com", class: "blue", target: "_blank"}, "odnoklassniki.com"), ");")
	                    ), 
	                    React.createElement("li", null, 
	                      React.createElement("p", null, "Запрещается повторная регистрация одного физического лица под различными email-аккаунтами;")
	                    )
	                  )
	                ), 
	                React.createElement("li", null, 
	                  React.createElement("p", null, "Сроки проведения Акции: 2 августа – 14 августа 2016 года. Сроки могут быть изменены по решению" + " " +
	                    "Организатора без предварительного уведомления. ")
	                ), 
	                React.createElement("li", null, 
	                  React.createElement("p", null, "Порядок подведения итогов Акции:"), 
	                  React.createElement("ul", null, 
	                    React.createElement("li", null, React.createElement("p", null, "Дата подведения итогов: 15 августа 2016 года.")), 
	                    React.createElement("li", null, React.createElement("p", null, "Победитель Акции определяется генератором случайных чисел среди участников, выполнивших все" + " " +
	                      "условия Акции.")), 
	                    React.createElement("li", null, React.createElement("p", null, "Информация о результатах публикуется в социальных сетях OneTwoTrip."), 
	                      React.createElement("p", null, "Победитель Акции" + " " +
	                        "уведомляется о выигрыше по e-mail. Организатор не несет ответственности за любые сбои, влияющие" + " " +
	                        "на" + " " +
	                        "получение победителем Акции указанного письма, а также за несанкционированный доступ третьих лиц" + " " +
	                        "к" + " " +
	                        "электронной почте победителя Акции."))
	                  )
	                ), 
	                React.createElement("li", null, 
	                  React.createElement("p", null, "Призы Акции:"), 
	                  React.createElement("ul", null, 
	                    React.createElement("li", null, React.createElement("p", null, "Главный приз Акции: два билета на любой поезд по России и за границу, которые можно купить на" + " " +
	                      "сайте или в приложении OneTwoTrip. Суммарная стоимость билетов не должна превышать 50 000 рублей," + " " +
	                      "класс вагона – сидячий, общий, плацкарт или купе. Даты поездки: не ранее 01.09.2016 и не позднее" + " " +
	                      "01.11.2016. По выбранному маршруту не предусматривается повышение класса вагона. Победитель" + " " +
	                      "получает приз в виде сертификата без права его передачи, обмена и возврата либо путем перечисления" + " " +
	                      "суммы, эквивалентной сумме приза, на расчетный счет Победителя Акции в целях покупки" + " " +
	                      "соответствующих билетов."))
	                  )
	                ), 
	                React.createElement("li", null, 
	                  React.createElement("p", null, "Порядок получения приза:"), 
	                  React.createElement("ul", null, 
	                    React.createElement("li", null, React.createElement("p", null, "Для получения приза Победитель Акции в течение 5 (пяти) рабочих дней с даты получения email с" + " " +
	                      "уведомлением от Организатора должен связаться с Организатором по электронной почте" + " " +
	                      "promo@onetwotrip.com с адреса электронной почты, указанного при заполнении теста, и сообщить свои" + " " +
	                      "данные для оформления билета.")), 
	                    React.createElement("li", null, React.createElement("p", null, "Организатор направляет Победителю Акции контрольные купоны, подтверждающие факт покупки" + " " +
	                      "билета, на электронную почту Победителя в течение 14 дней с момента предоставления Организатору" + " " +
	                      "данных для оформления билета. Передача Приза третьим лицам не допускается.")), 
	                    React.createElement("li", null, React.createElement("p", null, "В случае получения Победителем Акции приза в виде денежной суммы, Победитель Акции обязуется" + " " +
	                      "предоставить Организатору все документы, подтверждающие приобретение и использование Приза в" + " " +
	                      "целях, предусмотренных настоящими Правилами. В случае несоблюдения данного требования Победитель" + " " +
	                      "Акции обязуется возвратить Организатору перечисленные средства в полном объеме по первому" + " " +
	                      "требованию Организатора.")), 
	                    React.createElement("li", null, React.createElement("p", null, "Все применимые налоги и платежи, связанные с получением и использованием Приза, любые сборы," + " " +
	                      "устанавливаемые государственными органами страны, имеющей отношение к поездке, и любые другие" + " " +
	                      "личные расходы являются зоной ответственности Победителя Акции. Победитель Акции самостоятельно" + " " +
	                      "несет ответственность по любым претензиям, которые связаны с получением и использованием" + " " +
	                      "Приза.")), 
	                    React.createElement("li", null, React.createElement("p", null, "Дополнительные призы: Промо-коды на сумму 1500 рублей каждый на оплату первого бронирования" + " " +
	                      "номера в одном из отелей, представленных на Сайте Организатора."), 
	                      React.createElement("ul", null, 
	                        React.createElement("li", null, React.createElement("p", null, "Промо-код необходимо активировать не позднее 1 ноября 2016 года. Получение Приза в" + " " +
	                          "денежном эквиваленте, его обмен, продажа, а также использование в любых целях, не" + " " +
	                          "предусмотренных настоящими Правилами, не предусмотрены.")), 
	                        React.createElement("li", null, React.createElement("p", null, "Промо-код не суммируется с другими промокодами Организатора. Промо-код может быть" + " " +
	                          "использован только для оплаты бронирования стоимостью не менее 10 000 рублей.")), 
	                        React.createElement("li", null, React.createElement("p", null, "При отмене транзакции, для оплаты которой были использованы указанные промо-коды," + " " +
	                          "промо-коды восстановлению не подлежат. "))
	                      )
	                    )
	                  )
	                ), 
	                React.createElement("li", null, 
	                  React.createElement("p", null, "В случае возникновения причины, в том числе неконтролируемой Организатором Акции, которая" + " " +
	                    "препятствует должному проведению Акции, затрагивает ее безопасность, честность и целостность," + " " +
	                    "Организатор Акции вправе в одностороннем порядке изменить условия или временно прекратить проведение" + " " +
	                    "Акции без указания причин и направления уведомления.")
	                ), 
	                React.createElement("li", null, 
	                  React.createElement("p", null, "В случае, если в течение 5 (пяти) рабочих дней с даты получения email о выигрыше Победитель Акции" + " " +
	                    "не свяжется с Организатором, а также в случае добровольного отказа от Приза, Победитель Акции" + " " +
	                    "лишается права на получение Приза, как и компенсации в любой форме. При этом Организатор вправе" + " " +
	                    "наградить Призом иного участника Акции, выполнившего все условия Акции, либо распорядиться Призом" + " " +
	                    "иным способом по своему усмотрению.")
	                ), 
	                React.createElement("li", null, React.createElement("p", null, "Организатор Акции не несёт ответственности за надлежащее исполнение компанией-перевозчиком" + " " +
	                  "условий договора перевозки пассажира.")), 
	                React.createElement("li", null, React.createElement("p", null, "Организатор не несет ответственности за любые убытки, понесенные участниками Акции и Победителем" + " " +
	                  "Акции в связи с участием в Акции, получением и использованием Приза.")), 
	                React.createElement("li", null, React.createElement("p", null, "Отношения Победителя Акции с перевозчиком регулируются правилами соответствующего перевозчика." + " " +
	                  "Победитель Акции обязуется соблюдать все условия, установленные действующим законодательством и" + " " +
	                  "правилами соответствующей компании-перевозчика.")), 
	                React.createElement("li", null, React.createElement("p", null, "Победитель Акции обязуется сохранять в тайне всю конфиденциальную информацию, полученную от" + " " +
	                  "Организатора и касающуюся деятельности Организатора, порядка проведения Акции, получения и" + " " +
	                  "использования Приза, кроме информации, опубликованной на Сайте. ")), 
	                React.createElement("li", null, React.createElement("p", null, "Настоящие Правила, Акция не являются публичной офертой. Участие в Акции не означает заключение" + " " +
	                  "договора в результате объявления публичной оферты, не влечёт каких-либо обязательных отношений между" + " " +
	                  "Организатором и участниками Акции.")), 
	                React.createElement("li", null, React.createElement("p", null, "В случае нарушения участником правил и условий Акции, выявлении Организатором мошеннических" + " " +
	                  "операций участника с целью получения Приза, Организатор оставляет за собой право отказать в" + " " +
	                  "предоставлении Приза.")), 
	                React.createElement("li", null, React.createElement("p", null, "Принимая участие в Акции, участник даёт своё согласие на обработку своих персональных данных, а" + " " +
	                  "также на то, что его персональные данные могут быть переданы Организатором третьим лицам исключительно" + " " +
	                  "в целях проведения настоящей Акции.")), 
	                React.createElement("li", null, React.createElement("p", null, "Организатор не несет ответственность за технические сбои, в том числе за несвоевременную" + " " +
	                  "доставку/получение электронных писем, возникшие не по вине Организатора. "))
	              )
	            )
	          )
	        )
	      )
	    );
	  }
	});
	
	module.exports = Rules;


/***/ },
/* 5 */
/***/ function(module, exports) {

	/* WEBPACK VAR INJECTION */(function(global) {/*
	 *  Copyright (c) 2015-present, Facebook, Inc.
	 *  All rights reserved.
	 *
	 *  This source code is licensed under the BSD-style license found in the
	 *  LICENSE file in the root directory of this source tree. An additional grant
	 *  of patent rights can be found in the PATENTS file in the same directory.
	 *
	 */
	'use strict';
	
	var GLOBAL = typeof window === 'undefined' ? global : window;
	
	var setter = function(_setter, _clearer, array) {
	  return function(callback, delta) {
	    var id = _setter(function() {
	      _clearer.call(this, id);
	      callback.apply(this, arguments);
	    }.bind(this), delta);
	
	    if (!this[array]) {
	      this[array] = [id];
	    } else {
	      this[array].push(id);
	    }
	    return id;
	  };
	};
	
	var clearer = function(_clearer, array) {
	  return function(id) {
	    if (this[array]) {
	      var index = this[array].indexOf(id);
	      if (index !== -1) {
	        this[array].splice(index, 1);
	      }
	    }
	    _clearer(id);
	  };
	};
	
	var _timeouts = 'TimerMixin_timeouts';
	var _clearTimeout = clearer(GLOBAL.clearTimeout, _timeouts);
	var _setTimeout = setter(GLOBAL.setTimeout, _clearTimeout, _timeouts);
	
	var _intervals = 'TimerMixin_intervals';
	var _clearInterval = clearer(GLOBAL.clearInterval, _intervals);
	var _setInterval = setter(GLOBAL.setInterval, function() {/* noop */}, _intervals);
	
	var _immediates = 'TimerMixin_immediates';
	var _clearImmediate = clearer(GLOBAL.clearImmediate, _immediates);
	var _setImmediate = setter(GLOBAL.setImmediate, _clearImmediate, _immediates);
	
	var _rafs = 'TimerMixin_rafs';
	var _cancelAnimationFrame = clearer(GLOBAL.cancelAnimationFrame, _rafs);
	var _requestAnimationFrame = setter(GLOBAL.requestAnimationFrame, _cancelAnimationFrame, _rafs);
	
	var TimerMixin = {
	  componentWillUnmount: function() {
	    this[_timeouts] && this[_timeouts].forEach(function(id) {
	      GLOBAL.clearTimeout(id);
	    });
	    this[_timeouts] = null;
	    this[_intervals] && this[_intervals].forEach(function(id) {
	      GLOBAL.clearInterval(id);
	    });
	    this[_intervals] = null;
	    this[_immediates] && this[_immediates].forEach(function(id) {
	      GLOBAL.clearImmediate(id);
	    });
	    this[_immediates] = null;
	    this[_rafs] && this[_rafs].forEach(function(id) {
	      GLOBAL.cancelAnimationFrame(id);
	    });
	    this[_rafs] = null;
	  },
	
	  setTimeout: _setTimeout,
	  clearTimeout: _clearTimeout,
	
	  setInterval: _setInterval,
	  clearInterval: _clearInterval,
	
	  setImmediate: _setImmediate,
	  clearImmediate: _clearImmediate,
	
	  requestAnimationFrame: _requestAnimationFrame,
	  cancelAnimationFrame: _cancelAnimationFrame,
	};
	
	module.exports = TimerMixin;
	
	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx React.DOM */var React = __webpack_require__(1);
	var TimerMixin = __webpack_require__(5);
	
	var pfx = ["webkit", "moz", "MS", "o", ""];
	function PrefixedEvent(element, type, callback) {
	  for (var p = 0; p < pfx.length; p++) {
	    if (!pfx[p]) type = type.toLowerCase();
	    element.addEventListener(pfx[p] + type, callback, false);
	  }
	}
	
	var MainCard = React.createClass({displayName: "MainCard",
	  mixins              : [TimerMixin],
	  getInitialState     : function () {
	
	    return {
	      title        : 'Ответь на 5 вопросов и прими участие в розыгрыше 2х билетов на поезд.',
	      parallax     : 0.5,
	      playParallax : false,
	      hide         : false,
	      animate      : false,
	      display      : false
	    };
	  },
	  componentDidMount   : function () {
	    var self = this;
	    // PrefixedEvent(this.refs.main, 'AnimationEnd', function (event) {
	    //   self.stopAnimation();
	    // });
	    var st = this.state;
	    st.display = true;
	    this.setState(st);
	  },
	  stopAnimation       : function () {
	    var st = this.state;
	    st.playParallax = true;
	    this.setState(st);
	  },
	  setParallaxProgress : function (pr) {
	    var st = this.state;
	    st.parallax = Math.round(pr * 100) / 100;
	    this.setState(st);
	  },
	  getParallaxProgress : function () {
	    return this.state.parallax;
	  },
	  whenStart           : function (callback) {
	    this.onStart = callback;
	  },
	  start               : function (event) {
	    if (event) {
	      event.stopPropagation();
	      event.preventDefault();
	    }
	    this.hideLeft();
	    if (this.onStart) {
	      this.onStart();
	    }
	  },
	  hideLeft            : function () {
	    var st = this.state;
	    st.animate = true;
	    st.hide = true;
	    this.setState(st);
	    this.setTimeout(
	      function()  {
	        var s = this.state;
	        s.display = false;
	        this.setState(s);
	      }.bind(this),
	      400
	    );
	  },
	  render              : function () {
	
	    if (!this.state.hide && this.state.playParallax) {
	      var deg = this.state.parallax * 2 - 1;
	      var transform = 'rotateY(' + deg + "deg) translate(" + -1 * deg * 10 + "px, 0)";
	
	      var styles = {
	        'WebkitTransform' : transform,
	        'MozTransform'    : transform,
	        'MsTransform'     : transform,
	        'OTransform'      : transform,
	        'transform'       : transform
	      };
	
	    } else
	      styles = {};
	
	    styles.display = (this.state.display ? 'block' : 'none');
	
	    if (!this.state.hide && this.state.playParallax) {
	      var trPr = Math.min(Math.max(0, this.state.parallax), 1)
	        , airPr = Math.min(Math.max(0, 1 - this.state.parallax), 1);
	      var trZ = ((trPr > airPr) ? 1 : 0)
	        , airZ = ((trPr > airPr) ? 0 : 1);
	      var train_style = {
	        opacity : trPr,
	        zIndex  : trZ
	      }
	        , air_style = {
	        opacity : airPr,
	        zIndex  : airZ
	      };
	    } else {
	      train_style = {};
	      air_style = {};
	    }
	
	    var cl = "block-card" +
	      (this.state.hide ? " hide-left" : "") +
	      (this.state.animate ? " animate" : "");
	
	    return (
	      React.createElement("div", {className: cl, style: styles}, 
	
	        React.createElement("div", {className: "block-card__overflow"}, 
	          React.createElement("div", {className: "block-card__overflow__title"}, 
	            React.createElement("span", {className: "block-card__overflow__title__inner"}, "Конкурс завершен 14 августа 2016 года")
	          ), 
	          React.createElement("div", {className: "block-card__overflow__text"}, 
	            React.createElement("span", {className: "block-card__overflow__text__inner"}, "Если вам интересно посмотреть как это было, вы можете пройти конкурс еще раз или перейти к поиску путешествий.")
	          ), 
	
	          React.createElement("div", {className: "block-card__overflow__btns"}, 
	          
	            React.createElement("button", {onClick: this.start, onTouchStart: this.start, className: "block-card-main__footer__btn__start"}, 
	              React.createElement("span", null, "ПРОЙТИ ТЕСТ")
	            ), 
	
	            React.createElement("a", {href: "https://onetwotrip.com", className: "block-card__overflow__btns__btn block-card-main__footer__btn__start"}, 
	              React.createElement("span", null, "ПЕРЕЙТИ НА САЙТ")
	            )
	          )
	        ), 
	
	        React.createElement("div", {ref: "main", className: "block-card-main"}, 
	
	
	          React.createElement("div", {className: "block-card__content block-card-main__content"}, 
	            React.createElement("div", {className: "block-card-main__logo"}, 
	              React.createElement("img", {src: "./img/main/logo.png", alt: "vs", className: "block-card-main__logo__img"})
	            ), 
	            React.createElement("div", {className: "block-card-main__title"}, 
	              React.createElement("h2", {className: "block-card-main__title__heading"}, this.state.title)
	            ), 
	            React.createElement("div", {className: "block-card-main__or"}, 
	              React.createElement("div", {className: "block-main-or--is-left block-main-or block-main-air", style: air_style}, 
	                React.createElement("div", {className: "block-main-or__name"}, 
	                  React.createElement("span", {onClick: this.start, onTouchStart: this.start}, "самолет")
	                ), 
	                React.createElement("div", {className: "block-main-or__picture"}, 
	                  React.createElement("img", {src: "./img/main/img--air.png", alt: "", className: "block-main-or__picture__img"})
	                )
	              ), 
	              React.createElement("div", {className: "block-main-or--is-right block-main-or  block-main-train", style: train_style}, 
	                React.createElement("div", {className: "block-main-or__picture"}, 
	                  React.createElement("img", {src: "./img/main/img--train.png", alt: "", className: "block-main-or__picture__img"})
	                ), 
	                React.createElement("div", {className: "block-main-or__name"}, 
	                  React.createElement("span", {onClick: this.start, onTouchStart: this.start}, "поезд")
	                )
	              )
	            )
	          ), 
	          React.createElement("div", {className: "block-card__footer block-card-main__footer"}, 
	            React.createElement("div", {className: "block-card-main__footer__btn"}
	
	            )
	          )
	        )
	      )
	    );
	  }
	});
	
	module.exports = MainCard;


/***/ },
/* 7 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx React.DOM */var React = __webpack_require__(1);
	var TimerMixin = __webpack_require__(5);
	
	var Question = __webpack_require__(8)
	  , Answer = __webpack_require__(10);
	
	var Card = React.createClass({displayName: "Card",
	  mixins              : [TimerMixin],
	  getInitialState     : function () {
	    return {
	      animate   : true,
	      parallax  : 0,
	      answer    : false,
	      success   : true,
	      hideLeft  : false,
	      hideRight : true,
	      display   : false
	    };
	  },
	  show                : function () {
	    var st = this.state;
	    st.display = true;
	    st.hideRight = false;
	    this.setState(st);
	  },
	  hide                : function () {
	    var st = this.state;
	    st.hideRight = false;
	    st.hideLeft = true;
	    this.setState(st);
	    this.setTimeout(
	      function()  {
	        var s = this.state;
	        s.display = false;
	        this.setState(s);
	      }.bind(this),
	      400
	    );
	  },
	  componentDidMount   : function () {
	    //this.selectAir();
	  },
	  selectTrain         : function () {
	    var st = this.state;
	    st.answer = true;
	    this.refs.answer.setResult(this.props.data.train);
	    this.setState(st);
	
	    if (this.props.onAnswer)
	      this.props.onAnswer(this.props.data.train);
	  },
	  selectAir           : function () {
	    var st = this.state;
	    st.answer = true;
	    this.refs.answer.setResult(this.props.data.air);
	    this.setState(st);
	
	    if (this.props.onAnswer)
	      this.props.onAnswer(this.props.data.air);
	  },
	  setParallaxProgress : function (pr) {
	    var st = this.state;
	    st.parallax = Math.round(pr * 100) / 100;
	    this.setState(st);
	  },
	  getParallaxProgress : function () {
	    return this.state.parallax;
	  },
	  answerClick         : function () {
	    if (this.state.answer)
	      this.props.onAnswerClick();
	  },
	  render              : function () {
	    var classes = "block-card block-card-question-" + (this.props.index + 1)
	      + ((this.state.animate) ? " animate" : "")
	      + ((this.state.answer) ? " show-answer" : "")
	      + ((this.state.hideLeft) ? " hide-left" : "")
	      + ((this.state.hideRight) ? " hide-right" : "");
	
	    if (!this.state.hide && !this.state.hideRight && this.state.parallax != 0) {
	      var deg = (this.state.parallax * 2 - 1) * .5;
	      var transform = 'rotateY(' + deg + "deg) translate(" + -1 * deg * 10 + "px, 0)";
	
	      var styles = {
	        '-webkit-transform' : transform,
	        '-moz-transform'    : transform,
	        '-ms-transform'     : transform,
	        '-o-transform'      : transform,
	        'transform'         : transform
	      };
	
	    } else
	      styles = {};
	
	    styles.display = (this.state.display ? 'block' : 'none');
	
	    return (
	      React.createElement("div", {className: classes, style: styles}, 
	        React.createElement(Question, {back: this.props.data.back, number: this.props.data.number, 
	                  text: this.props.data.question, 
	                  onSelectTrain: this.selectTrain, onSelectAir: this.selectAir}), 
	        React.createElement(Answer, {ref: "answer", 
	                onClick: this.answerClick, 
	                back: this.props.data.back, text: this.props.data.answer, 
	                showTrain: this.props.data.train, showAir: this.props.data.air})
	      )
	    );
	  }
	});
	
	module.exports = Card;

/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx React.DOM */var React = __webpack_require__(1);
	
	var SelectButtons = __webpack_require__(9);
	
	var QuestionCard = React.createClass({displayName: "QuestionCard",
	  selectTrain : function () {
	    this.props.onSelectTrain();
	  },
	  selectAir   : function () {
	    this.props.onSelectAir();
	  },
	  render      : function () {
	    return (
	      React.createElement("div", {className: "block-card-question"}, 
	        React.createElement("div", {className: "block-card__content block-card-question__content"}, 
	          React.createElement("div", {className: "block-card-question__back"}, 
	            React.createElement("img", {src: this.props.back, alt: "", className: "block-card-question__back__img block-card-back-img"})
	          ), 
	          React.createElement("div", {className: "block-card-question__number"}, 
	            React.createElement("img", {src: this.props.number, alt: "", className: "block-card-question__number__img"})
	          ), 
	          React.createElement("div", {className: "block-card-question__task"}, 
	            React.createElement("div", {className: "block-card-question__task__title"}, 
	              this.props.text
	            ), 
	            React.createElement("div", {className: "block-card-question__task__select"}, 
	              "Выберите:"
	            ), 
	            React.createElement("div", {className: "block-card-question__task__btns"}, 
	              React.createElement(SelectButtons, {onSelectAir: this.selectAir, onSelectTrain: this.selectTrain})
	            )
	          )
	        )
	      )
	    );
	  }
	});
	
	module.exports = QuestionCard;

/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx React.DOM */var React = __webpack_require__(1);
	
	var SelectButtons = React.createClass({displayName: "SelectButtons",
	  render          : function () {
	    return (
	      React.createElement("div", {className: "block-card-select"}, 
	        React.createElement("button", {className: "block-card-select__btn block-card-select--air", onClick: this.props.onSelectAir}, 
	          React.createElement("span", {className: "block-card-select__btn__icon"}, 
	            React.createElement("img", {src: "./img/icon--air.png", alt: "", className: "block-card-select__btn__icon__img"})
	          ), 
	          React.createElement("span", {className: "block-card-select__btn__text"}, "Самолет")
	        ), 
	        React.createElement("button", {className: "block-card-select__btn block-card-select--train", onClick: this.props.onSelectTrain}, 
	          React.createElement("span", {className: "block-card-select__btn__icon"}, 
	            React.createElement("img", {src: "./img/icon--train.png", alt: "", className: "block-card-select__btn__icon__img"})
	          ), 
	          React.createElement("span", {className: "block-card-select__btn__text"}, "Поезд")
	        )
	      )
	    );
	  }
	});
	
	module.exports = SelectButtons;

/***/ },
/* 10 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx React.DOM */var React = __webpack_require__(1);
	
	var AnswerCard = React.createClass({displayName: "AnswerCard",
	  getInitialState : function () {
	    return {
	      success : true
	    };
	  },
	  setResult       : function (result) {
	    this.setState({
	      success : result
	    });
	  },
	  click           : function () {
	    this.props.onClick();
	  },
	  render          : function () {
	    return (
	      React.createElement("div", {className: "block-card-answer", onClick: this.click}, 
	        React.createElement("div", {className: "block-card-answer__back"}, 
	          React.createElement("img", {src: this.props.back, alt: "", className: "block-card-answer__back__img block-card-back-img"})
	        ), 
	        React.createElement("div", {className: "block-card-answer__icons"}, 
	          React.createElement("div", {className: "block-card-answer__icons__rights"}, 
	            (function() {
	              if (this.props.showAir) {
	                return (
	                  React.createElement("div", {className: "block-card-answer__icons__rights__icon"}, 
	                    React.createElement("img", {src: "./img/icon--air.png", alt: "", 
	                         className: "block-card-answer__icons__rights__icon__img"})
	                  )
	                );
	              }
	            }.bind(this))(), 
	            (function() {
	              if (this.props.showTrain) {
	                return (
	                  React.createElement("div", {className: "block-card-answer__icons__rights__icon"}, 
	                    React.createElement("img", {src: "./img/icon--train.png", alt: "", 
	                         className: "block-card-answer__icons__rights__icon__img"})
	                  )
	                );
	              }
	            }.bind(this))()
	          ), 
	          (function() {
	            if (this.state.success) {
	              return (
	                React.createElement("div", {className: "block-card-answer__icons__result"}, 
	                  React.createElement("img", {src: "./img/questions/q_answer--success.png", alt: "", 
	                       className: "block-card-answer__icons__result__img"})
	                )
	              );
	            } else {
	              return (
	                React.createElement("div", {className: "block-card-answer__icons__result"}, 
	                  React.createElement("img", {src: "./img/questions/q_answer--fail.png", alt: "", 
	                       className: "block-card-answer__icons__result__img"})
	                )
	              );
	            }
	          }.bind(this))()
	        ), 
	        React.createElement("div", {className: "block-card-answer__desc"}, 
	          React.createElement("div", {className: "block-card-answer__desc__text"}, this.props.text)
	        )
	      )
	    );
	  }
	});
	
	module.exports = AnswerCard;

/***/ },
/* 11 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx React.DOM */var React = __webpack_require__(1);
	
	function getNumberImage(num) {
	  switch (num) {
	    case 1:
	      return (React.createElement("img", {src: "./img/finish/num-1.png"}));
	    case 2:
	      return (React.createElement("img", {src: "./img/finish/num-2.png"}));
	    case 3:
	      return (React.createElement("img", {src: "./img/finish/num-3.png"}));
	    case 4:
	      return (React.createElement("img", {src: "./img/finish/num-4.png"}));
	    case 5:
	      return (React.createElement("img", {src: "./img/finish/num-5.png"}));
	    case 0:
	    default:
	      return (React.createElement("img", {src: "./img/finish/num-0.png"}));
	  }
	}
	
	var FinishCard = React.createClass({displayName: "FinishCard",
	  getInitialState : function () {
	    return {
	      right : (this.props.right_answer) ? this.props.right_answer : 0
	    };
	  },
	  addRight        : function () {
	    this.setState({
	      right : (this.state.right + 1)
	    });
	  },
	  render          : function () {
	
	    var topText = (this.state.right <= 3)
	      ? 'Не расстраивайся, зато теперь ты знаешь ответы на самые коварные вопросы о поездах и самолетах!'
	      : 'Отлично! Ты наверное пилот или машинист. В любом случае, ты просто профи во всём, что касается поездов и самолётов.';
	
	    // var socialText = 'Расскажи об этом своим друзьям и выиграй 2 билета на поезд или промо код на бронирование отелей на 1500 рублей!';
	    var socialText = 'Расскажи об этом своим друзьям!';
	
	    return (
	      React.createElement("div", {className: "block-card"}, 
	        React.createElement("div", {className: "block-card-finish"}, 
	          React.createElement("div", {className: "block-card__content block-card-finish__content"}, 
	            React.createElement("div", {className: "block-card-finish__result"}, 
	              React.createElement("div", {className: "block-card-finish__result__head"}, 
	                React.createElement("img", {src: "./img/finish/logo.png", alt: "", className: "block-card-finish__result__head__logo"}), 
	
	                React.createElement("div", {className: "block-card-finish__result__head__icons"}, 
	                  React.createElement("div", {className: "block-card-finish__result__head__icons__icon"}, 
	                    React.createElement("img", {src: "./img/icon--air.png", alt: "", 
	                         className: "block-card-finish__result__head__icons__icon__img"})
	                  ), 
	                  React.createElement("div", {className: "block-card-finish__result__head__icons__delimiter"}, 
	                    "vs"
	                  ), 
	                  React.createElement("div", {className: "block-card-finish__result__head__icons__icon"}, 
	                    React.createElement("img", {src: "./img/icon--train.png", alt: "", 
	                         className: "block-card-finish__result__head__icons__icon__img"})
	                  )
	                )
	              ), 
	              React.createElement("div", {className: "block-card-finish__result__content"}, 
	                React.createElement("div", {className: "block-finish-result"}, 
	                  React.createElement("div", {className: "block-finish-result__right"}, 
	                    getNumberImage(this.state.right)
	                  ), 
	                  React.createElement("div", {className: "block-finish-result__delimiter"}, "/"), 
	                  React.createElement("div", {className: "block-finish-result__all"}, 
	                    getNumberImage(this.props.count)
	                  )
	                ), 
	                React.createElement("div", {className: "block-card-finish__result__content__title"}, "Первым делом самолеты!"), 
	                React.createElement("div", {className: "block-card-finish__result__content__text"}, topText)
	              )
	            ), 
	            React.createElement("div", {className: "block-card-finish__share"}, 
	              React.createElement("span", {className: "block-card-finish__share__text"}, socialText), 
	              React.createElement("span", {className: "block-card-finish__share__social"}, 
	                React.createElement("a", {href: "#", "data-social": "fb", "data-result": "true", 
	                   className: "block-card-finish__share__social__icon block-btn-social block-btn-social_fb"}, 
	                  React.createElement("span", {className: "block-btn-social__icon"})
	                ), 
	                React.createElement("a", {href: "#", "data-social": "vk", "data-result": "true", 
	                   className: "block-card-finish__share__social__icon block-btn-social block-btn-social_vk"}, 
	                  React.createElement("span", {className: "block-btn-social__icon"})
	                ), 
	                React.createElement("a", {href: "#", "data-social": "tw", "data-result": "true", 
	                   className: "block-card-finish__share__social__icon block-btn-social block-btn-social_tw"}, 
	                  React.createElement("span", {className: "block-btn-social__icon"})
	                )
	              )
	            )
	          )
	        )
	      )
	    );
	  }
	});
	
	module.exports = FinishCard;

/***/ },
/* 12 */
/***/ function(module, exports) {

	/** @jsx React.DOM */var path = '/promo/railways2016/';
	var currentDomain = 'http://www.onetwotrip.com';
	
	var config = {
	  confirm     : 'https://www.onetwotrip.com/_api/visitormanager/registerPromo/',
	  valideEmail : 'https://www.onetwotrip.com/_api/emailvalidator/validate/',
	  url         : currentDomain + path,
	  redirectURL : currentDomain + path + 'confirm.html',
	  source      : 'TA_RZHDVSAVIA'
	};
	
	var Final = (function () {
	  'use strict';
	
	  return React.createClass({
	    getInitialState     : function () {
	      return {
	        done    : false,
	        error   : false,
	        disable : false,
	        form    : true
	      };
	    },
	    componentDidMount   : function () {
	
	    },
	    form                : function () {
	      var st = this.state;
	      st.form = true;
	      this.setState(st);
	    },
	    email               : function (event) {
	      this.disable();
	      event.stopPropagation();
	      event.preventDefault();
	      var email = this.refs.email_input.value;
	
	      if (this.validateEmail(email)) {
	        this.validateEmailByAjax(email);
	      } else
	        this.formError();
	
	    },
	    validateEmail       : function (email) {
	      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	      return re.test(email);
	    },
	    validateEmailByAjax : function (email) {
	      jQuery.ajax({
	        url      : config.valideEmail,
	        data     : {
	          email : email
	        },
	        dataType : "jsonp"
	      }).done(this.__emailCheckAjax);
	
	      if (ga)
	        ga('send', 'event', 'rails2016', 'email_sent', email);
	    },
	    __emailCheckAjax    : function (response) {
	      if (response && response.isValid) {
	        jQuery.ajax({
	            url      : config.confirm,
	            data     : {
	              source      : config.source,
	              email       : this.refs.email_input.value,
	              redirectURL : config.redirectURL
	            },
	            dataType : "jsonp"
	          })
	          .done(this.done);
	      } else
	        this.formError();
	    },
	    formError           : function () {
	      this.undisable();
	      var st = this.state;
	      st.error = true;
	      this.setState(st);
	    },
	    formClear           : function () {
	      var st = this.state;
	      st.error = false;
	      this.setState(st);
	    },
	    done                : function (response) {
	      // console.log(response);
	      if (typeof response == 'string')
	        response = JSON.parse(response);
	      if (!response || response.result != 'OK') {
	        this.formError();
	      } else {
	        window.location = config.redirectURL;
	      }
	    },
	    disable             : function () {
	      var st = this.state;
	      st.disable = true;
	      this.setState(st);
	    },
	    undisable           : function () {
	      var st = this.state;
	      st.disable = false;
	      this.setState(st);
	    },
	    render              : function () {
	      return (
	        React.createElement("div", {className: "block-email"}, 
	          React.createElement("div", {className: "block-email__title"}, 
	            React.createElement("span", {className: "block-email__title__inner"}, "Спасибо за участие!")
	          ), 
	          React.createElement("form", {onSubmit: this.email, 
	                onInput: this.formClear, 
	                ref: "email_form", method: "post", 
	                className: "block-email__form"+(this.state.error?' error':"")}, 
	            React.createElement("input", {type: "hidden", name: "source", value: "promo_euro2016"}), 
	            React.createElement("input", {type: "hidden", name: "redirectURL", value: "https://www.onetwotrip.com/promo/newyear2016/confirm.html"}), 
	            React.createElement("div", {className: "block-email__form__desc"}, 
	              React.createElement("span", {className: "block-email__form__desc__inner"}, "Оставьте свой электронный адрес и узнайте о результатах соревнования")
	            ), 
	            React.createElement("div", {className: "block-email__form__control"}, 
	              React.createElement("input", {ref: "email_input", 
	                     type: "text", name: "email", 
	                     placeholder: "Введите электронный адрес", 
	                     className: "block-email__form__control__input"})
	            ), 
	            React.createElement("div", {className: "block-email__form__submit"}, 
	              React.createElement("button", {className: "block-email__form__submit__btn", 
	                      disabled: this.state.disable ? 'disabled' : ''}, 
	                React.createElement("span", null, "Отправить")
	              )
	            )
	          )
	        )
	      );
	    }
	  });
	}());
	
	module.exports = Final;


/***/ },
/* 13 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx React.DOM */var React = __webpack_require__(1);
	
	var NavCircle = __webpack_require__(14);
	
	var Footer = React.createClass({displayName: "Footer",
	
	  getInitialState : function () {
	    this.current = -1;
	    return {
	      end          : false,
	      disable_next : true,
	      showNav      : false
	    };
	  },
	
	  next       : function () {
	    if (this.current >= 0 && this.current < this.props.count)
	      this.refs['circle_' + this.current].done();
	    this.current++;
	    if (this.current >= 0 && this.current < this.props.count) {
	      this.showNav();
	      this.refs['circle_' + this.current].current();
	    } else
	      this.hideNav();
	
	  },
	  showNav    : function () {
	    this.setState({
	      end          : this.state.end,
	      disable_next : true,
	      showNav      : true
	    });
	  },
	  hideNav    : function () {
	    this.setState({
	      end          : this.state.end,
	      disable_next : true,
	      showNav      : false
	    });
	  },
	  enableNext : function (end) {
	    this.setState({
	      end          : ((typeof end !== 'undefined') ? end : this.state.end),
	      disable_next : false,
	      showNav      : true
	    });
	  },
	  clickNext  : function (event) {
	    if (!this.state.disable_next) {
	      event.stopPropagation();
	      event.preventDefault();
	      if (this.props.onNextClick)
	        this.props.onNextClick();
	    }
	  },
	  render     : function () {
	    var circles = [];
	
	    for (var i = 0; i < this.props.count; i++) {
	      circles.push((
	        React.createElement("li", {key: i, className: "block-cards__footer__nav__items__item"}, 
	          React.createElement(NavCircle, {ref: "circle_"+i, current: i === this.current, done: i < this.current})
	        )
	      ));
	    }
	
	    var classForFooter = 'block-cards__footer'
	      + (this.state.showNav ? ' show-nav' : '')
	      + (!this.state.disable_next ? ' show-next' : '');
	
	    var btn_text = (this.state.end)?'Результат':'Следующий';
	
	    return (
	
	      React.createElement("div", {className: classForFooter}, 
	        React.createElement("div", {className: "block-cards__footer__next"}, 
	          React.createElement("button", {className: "block-cards__footer__next__btn", 
	                  disabled: this.state.disable_next, 
	                  onTouchStart: this.clickNext, 
	                  onClick: this.clickNext}, 
	            React.createElement("span", null, btn_text)
	          )
	        ), 
	        React.createElement("div", {className: "block-cards__footer__nav"}, 
	          React.createElement("ul", {className: "block-cards__footer__nav__items"}, 
	            circles
	          )
	        ), 
	        React.createElement("div", {className: "block-cards__footer__links"}, 
	          React.createElement("a", {href: "#", "data-link": "rules", className: "block-cards__footer__links__link"}, "Правила участия")
	        )
	      )
	    );
	  }
	});
	
	module.exports = Footer;


/***/ },
/* 14 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx React.DOM */var React = __webpack_require__(1);
	
	var NavCircle = React.createClass({displayName: "NavCircle",
	  getInitialState : function () {
	    return {
	      current : !!this.props.current,
	      done    : !!this.props.done
	    };
	  },
	  current         : function () {
	    var st = this.state;
	    st.current = true;
	    this.setState(st);
	  },
	  done            : function () {
	    var st = this.state;
	    st.current = false;
	    st.done = true;
	    this.setState(st);
	
	  },
	  render          : function () {
	    var cl = "block-nav-circle"
	    + ((this.state.current) ? " block-nav-circle--is-current" : "")
	    + ((this.state.done) ? " block-nav-circle--is-done" : "");
	
	    return (
	      React.createElement("div", {className: cl})
	    )
	  }
	});
	
	module.exports = NavCircle;

/***/ },
/* 15 */
/***/ function(module, exports) {

	module.exports = {
	  questions : [
	    {
	      number   : './img/questions/q1--number.png',
	      back     : './img/questions/q1--back.png',
	      question : 'Путешествие из Петербурга в Москву: на чем быстрее сгонять в Большой?',
	      train    : true,
	      air      : true,
	      answer   : 'Оба ответа принимаются! Длительность путешествия из Петербурга в Москву зависит от того, сколько собираться и на чем добираться. Но «Сапсан» и самолёт (плюс дорога до и от аэропорта) занимают примерно равное количество времени.'
	    },
	    {
	      number   : './img/questions/q2--number.png',
	      back     : './img/questions/q2--back.png',
	      question : 'Прокатиться с ветерком и побывать на обзорной экскурсии в 60 городах за 7 дней. Какой вид транспорта выбрать, чтобы всё успеть?',
	      train    : true,
	      air      : false,
	      answer   : 'Фирменный поезд «Россия» едет из Москвы во Владивосток — это самый длинный маршрут на планете. Время в пути 6 дней 23 часа и 22 минуты.'
	    },
	    {
	      number   : './img/questions/q3--number.png',
	      back     : './img/questions/q3--back.png',
	      question : 'Если устроить наземную гонку между поездом и пассажирским самолетом, кто придёт к финишу первым? Делаем ставки.',
	      train    : true,
	      air      : false,
	      answer   : 'Все, кто поставили на поезд, не прогадали. Самый быстрый из них, Шанхайский маглев  на магнитном подвесе, разгоняется до 430 км/ч, в то время как Аэробус А300 — лишь до 300 км/ч. Правда дальше самолёт взлетает и… но это уже не суть:)'
	    },
	    {
	      number   : './img/questions/q4--number.png',
	      back     : './img/questions/q4--back.png',
	      question : 'Какой вид транспорта выбрал Дэвид Боуи, чтобы отправиться на концерт в Японию?',
	      train    : true,
	      air      : false,
	      answer   : 'Возможно, не только из-за аэрофобии, но и из-за любви к романтическому позвякиванию стаканов, убаюкивающему стуку колес и мелодичному крику проводника, Дэвид Боуи проехал всю Евразию (и ещё обратно вернулся) на поезде.'
	    },
	    {
	      number   : './img/questions/q5--number.png',
	      back     : './img/questions/q5--back.png',
	      question : 'Что тяжелее, локомотив поезда или Ту-134 вместе с пассажирами?',
	      train    : true,
	      air      : false,
	      answer   : 'Локомотив, конечно. Он со своими 135 тоннами занимает достойное место среди чемпионов в тяжёлом весе, в то время как Ту-134 даже со всеми пассажирами и чемоданами дотягивает лишь до 47 тонн.'
	    }
	  ]
	};

/***/ }
/******/ ]);
//# sourceMappingURL=rzhd-competition.map