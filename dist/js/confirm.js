/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx React.DOM */var React = __webpack_require__(1)
	  , ReactDOM = __webpack_require__(2);
	 
	
	var Share = __webpack_require__(3);
	var Rules = __webpack_require__(4);
	 
	
	document.addEventListener('click', function (event) {
	  target = event.target;
	  while (target.parentNode && target != document.body) {
	    if (target.classList.contains('block-btn-social')) {
	      event.stopPropagation();
	      event.preventDefault();
	      var url = 'https://www.onetwotrip.com/promo/railways2016/';
	      var type = target.getAttribute('data-social');
	
	      if (type == 'tw') {
	        Share['tw'](url, 'Самолет VS Поезд. Проведем тест драйв и выясним, кто быстрее, больше и мощнее!');
	      } else
	        Share[type](url);
	
	      return false;
	    } else if (target.hasAttribute('data-link') && target.getAttribute('data-link') == 'rules') {
	      document.querySelector('.block-rules').classList.add('open');
	      return false;
	    }
	    target = target.parentNode;
	  }
	});
	
	
	
	ReactDOM.render(
	  React.createElement(Rules, null),
	  document.getElementById('block_modal')
	);


/***/ },
/* 1 */
/***/ function(module, exports) {

	module.exports = React;

/***/ },
/* 2 */
/***/ function(module, exports) {

	module.exports = ReactDOM;

/***/ },
/* 3 */
/***/ function(module, exports) {

	module.exports = (function () {
	  "use strict";
	
	  var share = {};
	
	  share.fb = function (url) {
	    popup('http://www.facebook.com/sharer.php?u=' + url);
	  };
	
	  share.tw = function (url, text) {
	    popup('http://twitter.com/share?url=' + url +
	      '&text=' + text +
	      '&hashtags=onetwotrip_poezda');
	  };
	
	  share.vk = function (url, text, title, image) {
	    if (image)
	      popup('http://vk.com/share.php?url=' + url +
	        '&description=' + text +
	        '&title=' + title +
	        '&image=' + image);
	    else if (title)
	      popup('http://vk.com/share.php?url=' + url +
	        '&description=' + text +
	        '&title=' + title);
	    else if (text)
	      popup('http://vk.com/share.php?url=' + url +
	        '&description=' + text);
	    else
	      popup('http://vk.com/share.php?url=' + url);
	
	  };
	
	  share.ok = function (url, text) {
	
	    popup('http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1' +
	      '&st._surl=' + url +
	      '&st.comments=' + text);
	
	  };
	
	  share.okUrl = function (url) {
	    popup('https://connect.ok.ru/offer?url=' + url);
	  };
	
	  var popup = function (url) {
	
	    var left = (window.innerWidth - 626) / 2
	      , top = (window.innerHeight - 436) / 2;
	
	    left = (left < 0) ? 0 : left;
	    top = (top < 0) ? 0 : top;
	
	    window.open(url, '', 'toolbar=0,status=0,width=626,height=436,left=' + left + ',top=' + top);
	
	  };
	
	  return share;
	}());
	


/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx React.DOM */'use strict';
	
	var React = __webpack_require__(1);
	var ReactDOM = __webpack_require__(2);
	
	var Rules = React.createClass({displayName: "Rules",
	  componentDidMount : function () {
	    var self = this;
	    window.addEventListener('keydown', function (event) {
	      if (event.keyCode == 27)
	        self.hide();
	    });
	  },
	  hide              : function () {
	    document.querySelector('.block-rules').classList.remove('open');
	  },
	  click             : function (event) {
	    var target = event.target;
	    while (!$(target).hasClass('block-rules')) {
	      if ($(target).hasClass('block-rules__content__window'))
	        return true;
	      target = target.parentNode;
	    }
	    this.hide();
	  },
	  render            : function () {
	    return (
	      React.createElement("div", {className: "block-rules", onClick: this.click}, 
	        React.createElement("div", {className: "block-rules__back"}), 
	        React.createElement("div", {className: "block-rules__content"}, 
	          React.createElement("div", {className: "block-rules__content__window"}, 
	            React.createElement("div", {className: "block-rules__content__window__body"}, 
	              React.createElement("div", {className: "block-rules-close", onClick: this.hide}, "×"), 
	              React.createElement("h2", null, "Условия участия в Акции"), 
	              React.createElement("ul", null, 
	                React.createElement("li", null, 
	                  React.createElement("p", null, "Настоящие правила (далее – «Правила») регламентируют порядок участия в акции «Самолет или поезд»" + " " +
	                    "(далее – «Акция»).")
	                ), 
	                React.createElement("li", null, 
	                  React.createElement("p", null, "Организатором Акции является компания OneTwoTriр, далее именуемая Организатор.")
	                ), 
	                React.createElement("li", null, 
	                  React.createElement("p", null, "Акция не является лотерей или иным мероприятием, основанным на риске, и носит исключительно" + " " +
	                    "информационный характер о сайте ", React.createElement("a", {href: "https://www.onetwotrip.com", class: "blue", target: "_blank"}, "www.onetwotrip.com"), " " + " " +
	                    "и" + " " +
	                    "мобильном приложении OneTwoTrip (далее – «Сайт»). ")
	                ), 
	                React.createElement("li", null, 
	                  React.createElement("p", null, "Условия участия в Акции:"), 
	                  React.createElement("ul", null, 
	                    React.createElement("li", null, 
	                      React.createElement("p", null, "Участниками Акции могут являться только дееспособные физические совершеннолетние лица," + " " +
	                        "выполнившие условия ее проведения;")
	                    ), 
	                    React.createElement("li", null, 
	                      React.createElement("p", null, "Необходимо пройти тест на Сайте, оставить действующий адрес электронной почты и поделиться" + " " +
	                        "результатом теста в своем аккаунте в одной из социальных сетей (", 
	                        React.createElement("a", {href: "vk.com", class: "blue", target: "_blank"}, "vk.com"), ", ", 
	                        React.createElement("a", {href: "facebook.com", class: "blue", target: "_blank"}, "facebook.com"), ", ", 
	                        React.createElement("a", {href: "twitter.com", class: "blue", target: "_blank"}, "twitter.com"), ", ", 
	                        React.createElement("a", {href: "odnoklassniki.com", class: "blue", target: "_blank"}, "odnoklassniki.com"), ");")
	                    ), 
	                    React.createElement("li", null, 
	                      React.createElement("p", null, "Запрещается повторная регистрация одного физического лица под различными email-аккаунтами;")
	                    )
	                  )
	                ), 
	                React.createElement("li", null, 
	                  React.createElement("p", null, "Сроки проведения Акции: 2 августа – 14 августа 2016 года. Сроки могут быть изменены по решению" + " " +
	                    "Организатора без предварительного уведомления. ")
	                ), 
	                React.createElement("li", null, 
	                  React.createElement("p", null, "Порядок подведения итогов Акции:"), 
	                  React.createElement("ul", null, 
	                    React.createElement("li", null, React.createElement("p", null, "Дата подведения итогов: 15 августа 2016 года.")), 
	                    React.createElement("li", null, React.createElement("p", null, "Победитель Акции определяется генератором случайных чисел среди участников, выполнивших все" + " " +
	                      "условия Акции.")), 
	                    React.createElement("li", null, React.createElement("p", null, "Информация о результатах публикуется в социальных сетях OneTwoTrip."), 
	                      React.createElement("p", null, "Победитель Акции" + " " +
	                        "уведомляется о выигрыше по e-mail. Организатор не несет ответственности за любые сбои, влияющие" + " " +
	                        "на" + " " +
	                        "получение победителем Акции указанного письма, а также за несанкционированный доступ третьих лиц" + " " +
	                        "к" + " " +
	                        "электронной почте победителя Акции."))
	                  )
	                ), 
	                React.createElement("li", null, 
	                  React.createElement("p", null, "Призы Акции:"), 
	                  React.createElement("ul", null, 
	                    React.createElement("li", null, React.createElement("p", null, "Главный приз Акции: два билета на любой поезд по России и за границу, которые можно купить на" + " " +
	                      "сайте или в приложении OneTwoTrip. Суммарная стоимость билетов не должна превышать 50 000 рублей," + " " +
	                      "класс вагона – сидячий, общий, плацкарт или купе. Даты поездки: не ранее 01.09.2016 и не позднее" + " " +
	                      "01.11.2016. По выбранному маршруту не предусматривается повышение класса вагона. Победитель" + " " +
	                      "получает приз в виде сертификата без права его передачи, обмена и возврата либо путем перечисления" + " " +
	                      "суммы, эквивалентной сумме приза, на расчетный счет Победителя Акции в целях покупки" + " " +
	                      "соответствующих билетов."))
	                  )
	                ), 
	                React.createElement("li", null, 
	                  React.createElement("p", null, "Порядок получения приза:"), 
	                  React.createElement("ul", null, 
	                    React.createElement("li", null, React.createElement("p", null, "Для получения приза Победитель Акции в течение 5 (пяти) рабочих дней с даты получения email с" + " " +
	                      "уведомлением от Организатора должен связаться с Организатором по электронной почте" + " " +
	                      "promo@onetwotrip.com с адреса электронной почты, указанного при заполнении теста, и сообщить свои" + " " +
	                      "данные для оформления билета.")), 
	                    React.createElement("li", null, React.createElement("p", null, "Организатор направляет Победителю Акции контрольные купоны, подтверждающие факт покупки" + " " +
	                      "билета, на электронную почту Победителя в течение 14 дней с момента предоставления Организатору" + " " +
	                      "данных для оформления билета. Передача Приза третьим лицам не допускается.")), 
	                    React.createElement("li", null, React.createElement("p", null, "В случае получения Победителем Акции приза в виде денежной суммы, Победитель Акции обязуется" + " " +
	                      "предоставить Организатору все документы, подтверждающие приобретение и использование Приза в" + " " +
	                      "целях, предусмотренных настоящими Правилами. В случае несоблюдения данного требования Победитель" + " " +
	                      "Акции обязуется возвратить Организатору перечисленные средства в полном объеме по первому" + " " +
	                      "требованию Организатора.")), 
	                    React.createElement("li", null, React.createElement("p", null, "Все применимые налоги и платежи, связанные с получением и использованием Приза, любые сборы," + " " +
	                      "устанавливаемые государственными органами страны, имеющей отношение к поездке, и любые другие" + " " +
	                      "личные расходы являются зоной ответственности Победителя Акции. Победитель Акции самостоятельно" + " " +
	                      "несет ответственность по любым претензиям, которые связаны с получением и использованием" + " " +
	                      "Приза.")), 
	                    React.createElement("li", null, React.createElement("p", null, "Дополнительные призы: Промо-коды на сумму 1500 рублей каждый на оплату первого бронирования" + " " +
	                      "номера в одном из отелей, представленных на Сайте Организатора."), 
	                      React.createElement("ul", null, 
	                        React.createElement("li", null, React.createElement("p", null, "Промо-код необходимо активировать не позднее 1 ноября 2016 года. Получение Приза в" + " " +
	                          "денежном эквиваленте, его обмен, продажа, а также использование в любых целях, не" + " " +
	                          "предусмотренных настоящими Правилами, не предусмотрены.")), 
	                        React.createElement("li", null, React.createElement("p", null, "Промо-код не суммируется с другими промокодами Организатора. Промо-код может быть" + " " +
	                          "использован только для оплаты бронирования стоимостью не менее 10 000 рублей.")), 
	                        React.createElement("li", null, React.createElement("p", null, "При отмене транзакции, для оплаты которой были использованы указанные промо-коды," + " " +
	                          "промо-коды восстановлению не подлежат. "))
	                      )
	                    )
	                  )
	                ), 
	                React.createElement("li", null, 
	                  React.createElement("p", null, "В случае возникновения причины, в том числе неконтролируемой Организатором Акции, которая" + " " +
	                    "препятствует должному проведению Акции, затрагивает ее безопасность, честность и целостность," + " " +
	                    "Организатор Акции вправе в одностороннем порядке изменить условия или временно прекратить проведение" + " " +
	                    "Акции без указания причин и направления уведомления.")
	                ), 
	                React.createElement("li", null, 
	                  React.createElement("p", null, "В случае, если в течение 5 (пяти) рабочих дней с даты получения email о выигрыше Победитель Акции" + " " +
	                    "не свяжется с Организатором, а также в случае добровольного отказа от Приза, Победитель Акции" + " " +
	                    "лишается права на получение Приза, как и компенсации в любой форме. При этом Организатор вправе" + " " +
	                    "наградить Призом иного участника Акции, выполнившего все условия Акции, либо распорядиться Призом" + " " +
	                    "иным способом по своему усмотрению.")
	                ), 
	                React.createElement("li", null, React.createElement("p", null, "Организатор Акции не несёт ответственности за надлежащее исполнение компанией-перевозчиком" + " " +
	                  "условий договора перевозки пассажира.")), 
	                React.createElement("li", null, React.createElement("p", null, "Организатор не несет ответственности за любые убытки, понесенные участниками Акции и Победителем" + " " +
	                  "Акции в связи с участием в Акции, получением и использованием Приза.")), 
	                React.createElement("li", null, React.createElement("p", null, "Отношения Победителя Акции с перевозчиком регулируются правилами соответствующего перевозчика." + " " +
	                  "Победитель Акции обязуется соблюдать все условия, установленные действующим законодательством и" + " " +
	                  "правилами соответствующей компании-перевозчика.")), 
	                React.createElement("li", null, React.createElement("p", null, "Победитель Акции обязуется сохранять в тайне всю конфиденциальную информацию, полученную от" + " " +
	                  "Организатора и касающуюся деятельности Организатора, порядка проведения Акции, получения и" + " " +
	                  "использования Приза, кроме информации, опубликованной на Сайте. ")), 
	                React.createElement("li", null, React.createElement("p", null, "Настоящие Правила, Акция не являются публичной офертой. Участие в Акции не означает заключение" + " " +
	                  "договора в результате объявления публичной оферты, не влечёт каких-либо обязательных отношений между" + " " +
	                  "Организатором и участниками Акции.")), 
	                React.createElement("li", null, React.createElement("p", null, "В случае нарушения участником правил и условий Акции, выявлении Организатором мошеннических" + " " +
	                  "операций участника с целью получения Приза, Организатор оставляет за собой право отказать в" + " " +
	                  "предоставлении Приза.")), 
	                React.createElement("li", null, React.createElement("p", null, "Принимая участие в Акции, участник даёт своё согласие на обработку своих персональных данных, а" + " " +
	                  "также на то, что его персональные данные могут быть переданы Организатором третьим лицам исключительно" + " " +
	                  "в целях проведения настоящей Акции.")), 
	                React.createElement("li", null, React.createElement("p", null, "Организатор не несет ответственность за технические сбои, в том числе за несвоевременную" + " " +
	                  "доставку/получение электронных писем, возникшие не по вине Организатора. "))
	              )
	            )
	          )
	        )
	      )
	    );
	  }
	});
	
	module.exports = Rules;


/***/ }
/******/ ]);
//# sourceMappingURL=confirm.map