module.exports = (function () {
  "use strict";

  var share = {};

  share.fb = function (url) {
    popup('http://www.facebook.com/sharer.php?u=' + url);
  };

  share.tw = function (url, text) {
    popup('http://twitter.com/share?url=' + url +
      '&text=' + text +
      '&hashtags=onetwotrip_poezda');
  };

  share.vk = function (url, text, title, image) {
    if (image)
      popup('http://vk.com/share.php?url=' + url +
        '&description=' + text +
        '&title=' + title +
        '&image=' + image);
    else if (title)
      popup('http://vk.com/share.php?url=' + url +
        '&description=' + text +
        '&title=' + title);
    else if (text)
      popup('http://vk.com/share.php?url=' + url +
        '&description=' + text);
    else
      popup('http://vk.com/share.php?url=' + url);

  };

  share.ok = function (url, text) {

    popup('http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1' +
      '&st._surl=' + url +
      '&st.comments=' + text);

  };

  share.okUrl = function (url) {
    popup('https://connect.ok.ru/offer?url=' + url);
  };

  var popup = function (url) {

    var left = (window.innerWidth - 626) / 2
      , top = (window.innerHeight - 436) / 2;

    left = (left < 0) ? 0 : left;
    top = (top < 0) ? 0 : top;

    window.open(url, '', 'toolbar=0,status=0,width=626,height=436,left=' + left + ',top=' + top);

  };

  return share;
}());

