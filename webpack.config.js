module.exports = {

  entry     : {
    'rzhd-competition' : './src/jsx/app.jsx',
    'confirm' : './src/jsx/confirm.jsx'
  },
  //devtool : 'source-map',
  output    : {
    filename          : '[name].js',
    sourceMapFilename : '[name].map'
  },
  devtool   : 'source-map',
  module    : {
    loaders : [
      {
        //tell webpack to use jsx-loader for all *.jsx files
        test   : /\.jsx$/,
        loader : 'jsx-loader?insertPragma=React.DOM&harmony'
      }
    ]
  },
  externals : {
    'react-dom' : 'ReactDOM',
    'react'     : 'React',
    'loader'    : 'Loader'
  },
  resolve   : {
    extensions : ['', '.js', '.jsx']
  }
};
